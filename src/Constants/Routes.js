export const STARTUP_SCREEN = '/start-up'

// AUTH SCREEN
export const LOGIN_SCREEN = '/login'
export const SIGNUP_SCREEN = '/signup'
export const FORGET_PASSWORD_SCREEN = '/forgot-password'
export const FORGET_PASSWORD_LINK_SCREEN = '/forgot-password-link'
export const RESET_PASSWORD_SCREEN = '/reset-password'
export const RESET_PASSWORD_SUCCESS_SCREEN = '/reset-password-success'
export const AUTH_CONTACT_SCREEN = '/auth-contact'
export const OTP_SCREEN = '/otp'
export const PROFILE_SCREEN = '/profile'
export const ONBOARD_SCREEN = '/onboard'

// APP SCREEN
export const APP_SCREEN = '/app-screen'
export const CREATE_EVENT_SCREEN = '/create-event'
export const OFFLINE_TASTING_SCREEN = '/offline-tasting'
export const HOME_SCREEN = '/home'
export const MEMBERSHIP_SCREEN = '/membership'
export const MEMBERSHIP_SUCCESS_SCREEN = '/membership-success'
export const EVENT_DETAILS_SCREEN = '/event-details'
