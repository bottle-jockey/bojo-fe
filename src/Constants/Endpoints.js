import { Config } from '../Config'

export const BASE_URL = Config.API_URL

// AUTH API
export const POST_LOGIN = `${BASE_URL}/account/login`
export const POST_SIGN_UP = `${BASE_URL}/account/signup`
export const POST_VERIFY_PHONE = `${BASE_URL}/account/verify-phone-number`
export const API_ACCOUNT = `${BASE_URL}/account/`
export const POST_HEART_BEAT = `${BASE_URL}/users/heartbeat/`

// FORGOT PASSWORD API
export const POST_PASSWORD_RESET = `${BASE_URL}/account/password_reset/`
export const POST_PASSWORD_RESET_CONFIRM = `${BASE_URL}/account/password_reset/confirm/`

// HOME PAGE API
export const GET_ARTICLES = `${BASE_URL}/bojo/`

// SAMPLE API
export const GET_ALL_TODOS = `${BASE_URL}/todos`
export const GET_ONE_TODO = `${BASE_URL}/todos/:todo_id`
