import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Login from '@/Containers/Auth/Login'
import ForgetPassword from '@/Containers/Auth/ForgetPassword'
import ForgetPasswordLink from '@/Containers/Auth/ForgetPasswordLink'
import ResetPassword from '@/Containers/Auth/ResetPassword'
import ResetPasswordSuccess from '@/Containers/Auth/ResetPasswordSuccess'
import NameDetails from '@/Containers/Auth/Signup/NameDetails'
import ContactDetails from '@/Containers/Auth/Signup/ContactDetails'
import OtpScreen from '@/Containers/Auth/Signup/OtpScreen'
import ProfileDetails from '@/Containers/Auth/Signup/ProfileDetails'
import {
  AUTH_CONTACT_SCREEN,
  FORGET_PASSWORD_LINK_SCREEN,
  FORGET_PASSWORD_SCREEN,
  LOGIN_SCREEN,
  ONBOARD_SCREEN,
  OTP_SCREEN,
  PROFILE_SCREEN,
  RESET_PASSWORD_SCREEN,
  RESET_PASSWORD_SUCCESS_SCREEN,
  SIGNUP_SCREEN,
  STARTUP_SCREEN,
} from '@/Constants/Routes'
import IndexOnboardContainer from '@/Containers/Onboard/Index'
import IndexStartupContainer from '@/Containers/Startup/Index'

const Stack = createStackNavigator()

const AuthNavigator = props => {
  return (
    <Stack.Navigator initialRouteName={STARTUP_SCREEN} headerMode={'none'}>
      <Stack.Screen name={STARTUP_SCREEN} component={IndexStartupContainer} />
      <Stack.Screen name={ONBOARD_SCREEN} component={IndexOnboardContainer} />
      <Stack.Screen name={LOGIN_SCREEN} component={Login} />
      <Stack.Screen name={FORGET_PASSWORD_SCREEN} component={ForgetPassword} />
      <Stack.Screen
        name={FORGET_PASSWORD_LINK_SCREEN}
        component={ForgetPasswordLink}
      />
      <Stack.Screen name={RESET_PASSWORD_SCREEN} component={ResetPassword} />
      <Stack.Screen
        name={RESET_PASSWORD_SUCCESS_SCREEN}
        component={ResetPasswordSuccess}
      />
      <Stack.Screen name={SIGNUP_SCREEN} component={NameDetails} />
      <Stack.Screen name={AUTH_CONTACT_SCREEN} component={ContactDetails} />
      <Stack.Screen name={OTP_SCREEN} component={OtpScreen} />
      <Stack.Screen name={PROFILE_SCREEN} component={ProfileDetails} />
    </Stack.Navigator>
  )
}

export default AuthNavigator
