import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import {
  HOME_SCREEN,
  MEMBERSHIP_SCREEN,
  MEMBERSHIP_SUCCESS_SCREEN,
  PROFILE_SCREEN,
  CREATE_EVENT_SCREEN,
  EVENT_DETAILS_SCREEN
} from '../../Constants/Routes'
import ProfileDetails from '../../Containers/Auth/Signup/ProfileDetails'
import PremiumMemberShipContainer from '../../Containers/Auth/Signup/Membership'
import PremiumMemberShipSuccess from '../../Containers/Auth/Signup/MembershipSuccess'
import HomeScreen from '../../Containers/HomeScreen/Index'
import CreateEventScreen from '../../Containers/HomeScreen/CreateEvent'
import EventDetailsScreen from '../../Containers/HomeScreen/CreateEvent/EventDetails'

const Stack = createStackNavigator()

const AppNavigator = props => {
  return (
    <Stack.Navigator headerMode={'none'}>
      <Stack.Screen name={HOME_SCREEN} component={HomeScreen} />
      <Stack.Screen name={CREATE_EVENT_SCREEN} component={CreateEventScreen} />
      <Stack.Screen name={PROFILE_SCREEN} component={ProfileDetails} />
      <Stack.Screen
        name={MEMBERSHIP_SCREEN}
        component={PremiumMemberShipContainer}
      />
      <Stack.Screen
        name={MEMBERSHIP_SUCCESS_SCREEN}
        component={PremiumMemberShipSuccess}
      />
      <Stack.Screen
        name={EVENT_DETAILS_SCREEN}
        component={EventDetailsScreen}
      />
    </Stack.Navigator>
  )
}

export default AppNavigator
