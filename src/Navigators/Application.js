import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import { navigationRef } from '@/Navigators/Root'
import { SafeAreaView, StatusBar } from 'react-native'
import { useTheme } from '@/Theme'
import AuthNavigator from '@/Navigators/Auth/AuthNavigator'
import AppNavigator from '@/Navigators/App/AppNavigator'
import { ONBOARD_SCREEN } from '@/Constants/Routes'
import { navigateAndSimpleReset } from '@/Navigators/Root'
import { AUTH_FAILED, AUTH_SUCCESS } from '../Constants/ActionTypes'

let MainNavigator

// @refresh reset
const ApplicationNavigator = () => {
  const { Layout, darkMode, NavigationTheme } = useTheme()
  const { colors } = NavigationTheme
  // const [isApplicationLoaded, setIsApplicationLoaded] = useState(false)
  const applicationIsLoading = useSelector(state => state.startup.loading)
  const authState = useSelector(state => state.auth)

  useEffect(() => {
    if (MainNavigator == null && !applicationIsLoading) {
      MainNavigator = require('@/Navigators/Main').default
      // setIsApplicationLoaded(true)
    }
  }, [applicationIsLoading])

  // on destroy needed to be able to reset when app close in background (Android)
  useEffect(
    () => () => {
      // setIsApplicationLoaded(false)
      MainNavigator = null
    },
    [],
  )

  useEffect(() => {
    if (authState.status === AUTH_FAILED) {
      navigateAndSimpleReset(ONBOARD_SCREEN)
    }
  }, [authState.status])

  return (
    <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
      <StatusBar barStyle={!darkMode ? 'light-content' : 'dark-content'} />
      {authState.status === AUTH_SUCCESS ? <AppNavigator /> : <AuthNavigator />}
    </NavigationContainer>
  )
}

export default ApplicationNavigator
