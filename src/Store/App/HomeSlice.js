import { createSlice } from '@reduxjs/toolkit'
import {
  VALUE_SUCCESS,
  VALUE_REQUESTED,
  VALUE_FAILED,
} from '../../Constants/ActionTypes'
import { homeArticlesAction } from './Actions/HomeAction'

const initialState = {
  status: 'initiated',
  data: [],
  error: null,
}

const handleGetArticlesSuccess = (state, action) => {
  state.status = VALUE_SUCCESS
  state.data = action.payload
  state.error = null
}
const handleGetArticlesPending = (state, action) => {
  state.status = VALUE_REQUESTED
  state.data = []
  state.error = null
}
const handleGetArticlesFailed = (state, action) => {
  state.status = VALUE_FAILED
  state.data = []
  state.error = action.error.message
}

/** @type {import('@reduxjs/toolkit').CaseReducer} */
const reducerActions = () => ({
  [homeArticlesAction.fulfilled]: handleGetArticlesSuccess,
  [homeArticlesAction.pending]: handleGetArticlesPending,
  [homeArticlesAction.rejected]: handleGetArticlesFailed,
})

const homeSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {},
  extraReducers: reducerActions(),
})

export default homeSlice
