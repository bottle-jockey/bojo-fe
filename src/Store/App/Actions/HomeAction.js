import { createAsyncThunk } from '@reduxjs/toolkit'
import { HOME_GET_ARTICLES } from '../../../Constants/ActionTypes'
import { getArticles } from '../../../Services/App/HomeService'

export const homeArticlesAction = createAsyncThunk(
  HOME_GET_ARTICLES,
  async values => {
    const response = await getArticles()
    return response.data.articles
  },
)
