import homeSlice from './HomeSlice'

export default {
  articles: homeSlice.reducer,
}
