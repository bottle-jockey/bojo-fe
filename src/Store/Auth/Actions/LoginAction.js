import { createAsyncThunk } from '@reduxjs/toolkit'

import {
  HEART_BEAT_REQUEST,
  LOGIN_REQUEST,
  LOGOUT_REQUEST,
} from '../../../Constants/ActionTypes'
import { postHeartBeat, postLogin } from '../../../Services/Auth/AuthService'
import { saveTokens, deleteTokens } from '../../../Utils/AuthUtil'

export const loginUserAction = createAsyncThunk(LOGIN_REQUEST, async values => {
  const response = await postLogin(values)

  await saveTokens(response.data.tokens)
  return response
})

export const postHeartBeatAction = createAsyncThunk(
  HEART_BEAT_REQUEST,
  async values => {
    const response = await postHeartBeat(values)
    return response
  },
)

export const deleteLogoutAction = createAsyncThunk(
  LOGOUT_REQUEST,
  async (_, thunkAPI) => {
    await deleteTokens()
    thunkAPI.dispatch(postHeartBeatAction({}))
  },
)
