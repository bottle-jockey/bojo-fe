import { createAsyncThunk } from '@reduxjs/toolkit'
import {
  OTP_VERIFY_REQUEST,
  PASSWORD_RESET_REQUEST,
  PROFILE_AUTH_REQUEST,
  SIGNUP_REQUEST,
  PASSWORD_RESET_CONFIRM_REQUEST,
} from '../../../Constants/ActionTypes'
import {
  postPasswordReset,
  postPasswordResetConfirm,
  postSignup,
  postVerifyPhone,
  putAccount,
} from '../../../Services/Auth/AuthService'
import { navigate, navigateAndSimpleReset } from '@/Navigators/Root'
import {
  HOME_SCREEN,
  OTP_SCREEN,
  PROFILE_SCREEN,
  RESET_PASSWORD_SCREEN,
  RESET_PASSWORD_SUCCESS_SCREEN,
} from '../../../Constants/Routes'
import { saveTokens } from '../../../Utils/AuthUtil'

export const signupAction = createAsyncThunk(SIGNUP_REQUEST, async values => {
  const response = await postSignup(values)

  navigate(OTP_SCREEN)
  return response
})

export const otpVerifyAction = createAsyncThunk(
  OTP_VERIFY_REQUEST,
  async values => {
    const response = await postVerifyPhone(values)
    await saveTokens(response.data.tokens)

    navigateAndSimpleReset(PROFILE_SCREEN)
    return response
  },
)

export const profileUpdateAction = createAsyncThunk(
  PROFILE_AUTH_REQUEST,
  async values => {
    const response = await putAccount(values)

    navigateAndSimpleReset(HOME_SCREEN)
    return response
  },
)

export const passwordResetAction = createAsyncThunk(
  PASSWORD_RESET_REQUEST,
  async values => {
    const response = await postPasswordReset(values)

    navigate(RESET_PASSWORD_SCREEN)
    return response
  },
)

export const passwordResetConfirmAction = createAsyncThunk(
  PASSWORD_RESET_CONFIRM_REQUEST,
  async values => {
    const response = await postPasswordResetConfirm(values)

    navigate(RESET_PASSWORD_SUCCESS_SCREEN)
    return response
  },
)
