import loginSlice from './Login'
import authSlice from './AuthSlice'
import signupSlice from './SignupSlice'
import onboardSlice from './OnboardSlice'

export default {
  login: loginSlice.reducer,
  auth: authSlice.reducer,
  signup: signupSlice.reducer,
  onboard: onboardSlice.reducer,
}
