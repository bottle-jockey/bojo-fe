import { createSlice } from '@reduxjs/toolkit'
import {
  LOGIN_SUCCESS,
  LOGIN_REQUESTED,
  LOGIN_FAILED,
  LOGIN_RESET,
} from '../../Constants/ActionTypes'
import { loginUserAction } from './Actions/LoginAction'

const initialState = {
  status: 'initiated',
  data: {},
  error: null,
}

const handleUserLoginSuccess = (state, action) => {
  state.status = LOGIN_SUCCESS
  state.data = action.payload
  state.error = null
}
const handleUserLoginPending = (state, action) => {
  state.status = LOGIN_REQUESTED
  state.data = {}
  state.error = null
}
const handleUserLoginFailed = (state, action) => {
  state.status = LOGIN_FAILED
  state.data = {}
  state.error = action.error.message
}

const handleStateReset = state => {
  state = initialState
  return state
}

/** @type {import('@reduxjs/toolkit').CaseReducer} */
const reducerActions = () => ({
  [loginUserAction.fulfilled]: handleUserLoginSuccess,
  [loginUserAction.pending]: handleUserLoginPending,
  [loginUserAction.rejected]: handleUserLoginFailed,
})

const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    [LOGIN_RESET]: handleStateReset,
  },
  extraReducers: reducerActions(),
})

export default loginSlice
