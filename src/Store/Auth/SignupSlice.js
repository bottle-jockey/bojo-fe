import { createSlice } from '@reduxjs/toolkit'
import {
  SIGNUP_SUCCESS,
  SIGNUP_REQUESTED,
  SIGNUP_FAILED,
} from '@/Constants/ActionTypes'
import {
  otpVerifyAction,
  passwordResetAction,
  passwordResetConfirmAction,
  profileUpdateAction,
  signupAction,
} from './Actions/SignupAction'
import {
  OTP_VERIFY_FAILED,
  OTP_VERIFY_REQUESTED,
  OTP_VERIFY_SUCCESS,
  VALUE_FAILED,
  VALUE_REQUESTED,
  VALUE_SUCCESS,
} from '../../Constants/ActionTypes'

const initialState = {
  status: 'initiated',
  data: {},
  error: null,
  otpVerifyStatus: 'initiated',
  profileStatus: 'initiated',
  passwordResetStatus: 'initiated',
  passwordResetConfirmStatus: 'initiated',
}

const handleSignupSuccess = (state, action) => {
  state.status = SIGNUP_SUCCESS
  state.data = action.payload.data
  state.error = null
}
const handleUserLoginPending = (state, action) => {
  state.status = SIGNUP_REQUESTED
  state.data = {}
  state.error = null
}
const handleUserLoginFailed = (state, action) => {
  state.status = SIGNUP_FAILED
  state.data = {}
  state.error = action.error.message
}

const handleOtpVerifyRequested = (state, action) => {
  state.otpVerifyStatus = OTP_VERIFY_REQUESTED
  state.error = null
}

const handleOtpVerifySuccess = (state, action) => {
  state.otpVerifyStatus = OTP_VERIFY_SUCCESS
  state.error = null
}

const handleOtpVerifyFailed = (state, action) => {
  state.otpVerifyStatus = OTP_VERIFY_FAILED
  state.error = action.error.message
}

const handleProfileUpdateSuccess = (state, action) => {
  state.profileStatus = VALUE_SUCCESS
  state.error = null
}

const handleProfileUpdateFailed = (state, action) => {
  state.profileStatus = VALUE_FAILED
  state.error = action.error.message
}

const handleProfileUpdatePending = (state, action) => {
  state.profileStatus = VALUE_REQUESTED
  state.error = null
}

const handlePasswordResetPending = state => {
  state.passwordResetStatus = VALUE_REQUESTED
  state.error = null
}

const handlePasswordResetSubmitFailed = (state, action) => {
  state.passwordResetStatus = VALUE_FAILED
  state.error = action.error.message
}

const handlePasswordResetSubmitSuccess = (state, action) => {
  state.passwordResetStatus = VALUE_SUCCESS
  state.error = null
}

const handlePasswordResetConfirmPending = state => {
  state.passwordResetConfirmStatus = VALUE_REQUESTED
  state.error = null
}

const handlePasswordResetConfirmFailed = (state, action) => {
  state.passwordResetConfirmStatus = VALUE_FAILED
  state.error = action.error.message
}

const handlePasswordResetConfirmSuccess = (state, action) => {
  state.passwordResetConfirmStatus = VALUE_SUCCESS
  state.error = null
}

/** @type {import('@reduxjs/toolkit').CaseReducer} */
const reducerActions = () => ({
  [signupAction.pending]: handleUserLoginPending,
  [signupAction.fulfilled]: handleSignupSuccess,
  [signupAction.rejected]: handleUserLoginFailed,
  [otpVerifyAction.pending]: handleOtpVerifyRequested,
  [otpVerifyAction.fulfilled]: handleOtpVerifySuccess,
  [otpVerifyAction.rejected]: handleOtpVerifyFailed,
  [profileUpdateAction.pending]: handleProfileUpdatePending,
  [profileUpdateAction.fulfilled]: handleProfileUpdateSuccess,
  [profileUpdateAction.rejected]: handleProfileUpdateFailed,
  [passwordResetAction.pending]: handlePasswordResetPending,
  [passwordResetAction.fulfilled]: handlePasswordResetSubmitSuccess,
  [passwordResetAction.rejected]: handlePasswordResetSubmitFailed,
  [passwordResetConfirmAction.pending]: handlePasswordResetConfirmPending,
  [passwordResetConfirmAction.fulfilled]: handlePasswordResetConfirmSuccess,
  [passwordResetConfirmAction.rejected]: handlePasswordResetConfirmFailed,
})

const signupSlice = createSlice({
  name: 'signup',
  initialState,
  reducers: {},
  extraReducers: reducerActions(),
})

export default signupSlice
