import { createSlice } from '@reduxjs/toolkit'
import {
  AUTH_SUCCESS,
  AUTH_FAILED,
  AUTH_REQUESTED,
} from '../../Constants/ActionTypes'
import { loginUserAction, postHeartBeatAction } from './Actions/LoginAction'
import { otpVerifyAction } from './Actions/SignupAction'

const initialState = {
  status: 'initiated',
  data: {},
  error: null,
}

const handleAuthSuccess = (state, action) => {
  state.status = AUTH_SUCCESS
  state.data = action.payload.data
  state.error = null
}
const handleAuthLoading = (state, action) => {
  state.status = AUTH_REQUESTED
  state.data = {}
  state.error = null
}
const handleAuthFailed = (state, action) => {
  state.status = AUTH_FAILED
  state.data = {}
  state.error = action.error.message
}

/** @type {import('@reduxjs/toolkit').CaseReducer} */
const reducerActions = () => ({
  [postHeartBeatAction.rejected]: handleAuthFailed,
  [postHeartBeatAction.fulfilled]: handleAuthSuccess,
  [postHeartBeatAction.pending]: handleAuthLoading,
  [loginUserAction.fulfilled]: handleAuthSuccess,
  [loginUserAction.rejected]: handleAuthFailed,
  [otpVerifyAction.fulfilled]: handleAuthSuccess,
})

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: reducerActions(),
})

export default authSlice
