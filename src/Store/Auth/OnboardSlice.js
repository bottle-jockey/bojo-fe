import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  complete: false,
}

const onboardSlice = createSlice({
  name: 'onboard',
  initialState,
  reducers: {
    markOnboardComplete: state => {
      state.complete = true
    },
  },
})

export default onboardSlice
