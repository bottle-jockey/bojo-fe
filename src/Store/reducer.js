import authReducer from './Auth/Index'
import appReducer from './App/index'

export const allReducers = {
  ...authReducer,
  ...appReducer,
}
