import React from "react";
import { View, Text } from "react-native";
import styles from "./style";


const ValidationError = props => {
    const renderError = () => {
        const { touched, warning, error, label = null } = props;
        return <View >
            {
                touched &&
                error && <Text style={styles.errorText}>{`${label || 'Field'} ${error}`}</Text>
            }
            {
                touched && !error &&
                warning && <Text style={styles.errorText}>{`${label || 'Field'} ${error}`}</Text>
            }
        </View>
    }

    return renderError()
}

export default ValidationError;