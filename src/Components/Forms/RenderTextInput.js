import React from 'react';
import { TextInput, View, Text } from 'react-native';

import styles from './style';
import ValidationError from './ValidationError';
import { Colors } from '@/Theme/Variables'

/**
 * to be wrapped with redux-form Field component
 */
export default function MyTextInput(props) {
  const { input, label, meta: {touched, error, warning}, ...inputProps } = props;

  // do not display warning if the field has not been touched or if it's currently being edited
  return (
    <View style={{width: '100%'}}>
      <TextInput
        {...inputProps}
        onChangeText={input.onChange}
        onBlur={input.onBlur}
        onFocus={input.onFocus}
        value={input.value}
        secureTextEntry={props.password}
        keyboardType={props.keyboardType}
        placeholderTextColor='#d7d4cd'
        style={(touched && error) ? { borderRadius: 10, textAlign: 'center', backgroundColor: '#fff', height: 50, borderColor: Colors.error, borderWidth: 1, color: '#000'} : { borderColor: '#D7D4CD', borderWidth: 0.5, borderRadius: 7, backgroundColor: '#fff', height: 50, textAlign: 'center', color: '#000'}}
      />
      <ValidationError touched={touched} warning={warning} error={error} label={label} />
    </View>
  );
}