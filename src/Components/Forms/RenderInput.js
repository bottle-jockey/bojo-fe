import PropTypes from 'prop-types'
import React from 'react'
import ValidationError from './ValidationError'
import BojoInputBox from '@/Assets/BojoInputs/BojoInputBox'

export default function RenderInput(props) {
  const {
    label,
    placeholder,
    password,
    meta: { touched, error, warning },
    input: { onChange, ...rest },
  } = props

  return (
    <>
      <BojoInputBox
        {...rest}
        onChangeText={onChange}
        placeholder={placeholder}
        password={password}
        error={error}
        touched={touched}
        styles={props.styles}
      />
      <ValidationError
        touched={touched}
        warning={warning}
        error={error}
        label={label}
      />
    </>
  )
}
