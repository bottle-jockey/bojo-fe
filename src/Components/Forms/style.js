import { Dimensions, Platform, StyleSheet } from "react-native";
import { Colors } from '@/Theme/Variables'


const width = Dimensions.get("screen").width;
const elementMaxWidth = width - (16 * 2);

export default StyleSheet.create({
    errorText: {
        alignSelf: 'flex-start',
        fontSize: 12,
        color: Colors.error,
        margin: 4
    },
});