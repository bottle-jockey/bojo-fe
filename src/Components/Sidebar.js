import React from 'react'
import { Text, TouchableOpacity, View, Image } from 'react-native'
import { Container, Icon } from 'native-base'
import styles from '../Assets/styles'
import { PROFILE_SCREEN } from '../Constants/Routes'
import BackArrow from '../Assets/Images/right-arrow.png'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import { deleteLogoutAction } from '../Store/Auth/Actions/LoginAction'

const SideBar = props => {
  const { t } = useTranslation()
  const dispatch = useDispatch()

  const handleLogout = () => {
    dispatch(deleteLogoutAction())
  }

  return (
    <Container style={[styles.paddingHorizontal25, styles.paddingVertical25]}>
      <TouchableOpacity
        style={styles.marginTop30}
        onPress={() => props.onCloseDrawer()}
      >
        <Image source={BackArrow} />
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.marginTop30, styles.marginLeft10]}
        onPress={() => props.navigation.navigate(PROFILE_SCREEN)}
      >
        <Text style={[styles.sideBarText, styles.fontWeightBold]}>
          {t('sideBar.myProfile')}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.marginTop30, styles.marginLeft10]}>
        <Text style={[styles.sideBarText, styles.fontWeightBold]}>
          {t('sideBar.tastingHistory')}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.marginTop30, styles.marginLeft10]}>
        <Text style={[styles.sideBarText, styles.fontWeightBold]}>
          {t('sideBar.premiumMembership')}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.marginTop30, styles.marginLeft10]}>
        <Text style={[styles.sideBarText, styles.fontWeightBold]}>
          {t('sideBar.orderKit')}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.marginTop30, styles.marginLeft10]}>
        <Text style={[styles.sideBarText, styles.fontWeightBold]}>
          {t('sideBar.printSilksCodes')}
        </Text>
      </TouchableOpacity>
      <View style={styles.screenPositionBottom1}>
        <View style={styles.screenPositionBottom2}>
          <TouchableOpacity style={[styles.marginTop30]}>
            <Text style={styles.sideBarText}>{t('sideBar.faqs')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.marginTop30]}>
            <Text style={styles.sideBarText}>{t('sideBar.termsOfUse')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.marginTop30]}>
            <Text style={styles.sideBarText}>{t('sideBar.privacyPolicy')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleLogout} style={[styles.marginTop30]}>
            <Text style={styles.sideBarText}>{t('sideBar.logout')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Container>
  )
}

export default SideBar
