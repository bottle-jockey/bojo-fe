import React, { useRef } from 'react'
import { View } from 'react-native'
import {
  Container,
  Drawer,
  Header,
  Body,
  Left,
  Right,
  Button,
  Icon,
  Content,
} from 'native-base'
import SideBar from './Sidebar'
import BojoLogo from '@/Assets/Images/BojoLogo.svg'
import { Colors } from '../Theme/Variables'

const AppStackWrapper = props => {
  const drawerRef = useRef(null)

  const closeDrawer = () => {
    drawerRef.current._root.close()
  }
  const openDrawer = () => {
    drawerRef.current._root.open()
  }
  return (
    <Drawer
      ref={drawerRef}
      side="left"
      acceptPan={true}
      panOpenMask={0.9}
      openDrawerOffset={0.3}
      captureGestures="open"
      content={
        <SideBar
          onCloseDrawer={() => closeDrawer()}
          navigation={props.navigation}
        />
      }
      onClose={() => closeDrawer()}
    >
      <Container>
        <Header style={{ backgroundColor: Colors.primary }}>
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => openDrawer()}>
              <Icon name="menu" style={{color: 'white'}} />
            </Button>
          </Left>
          <Body style={{ flex: 2, justifyContent: 'center' }}>
            <View style={{ alignSelf: 'center' }}>
              <BojoLogo height={48} />
            </View>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>
        <Content>{props.children}</Content>
      </Container>
    </Drawer>
  )
}
export default AppStackWrapper
