import React from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { Brand } from '@/Components'
import { useTheme } from '@/Theme'
import { useTranslation } from 'react-i18next'
import { Container } from 'native-base'

const IndexExampleContainer = () => {
  const { t } = useTranslation()
  const { Fonts, Gutters, Layout } = useTheme()

  return (
    <Container>
      <View style={[Layout.fill, Layout.colCenter, Gutters.smallHPadding]}>
        <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
          <Brand />
          <ActivityIndicator size={'large'} style={[Gutters.largeVMargin]} />
          <Text style={Fonts.textCenter}>{t('home.welcome')}</Text>
        </View>
      </View>
    </Container>
  )
}

export default IndexExampleContainer
