import { Container, Content, Text, Header, Footer } from 'native-base'
import React from 'react'

const AppScreen = props => {
  return (
    <Container>
      <Header />
      <Content>
        <Text>Contents start here</Text>
      </Content>
      <Footer>
        <Text style={{ color: 'white' }}>Footer</Text>
      </Footer>
    </Container>
  )
}

export default AppScreen
