import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import { useTheme } from '@/Theme'
import { Container, Content } from 'native-base'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import RenderInput from '@/Components/Forms/RenderInput'
import { required, email } from '@/Utils/Validation'
import { Field, reduxForm } from 'redux-form'
import styles from '../../Assets/styles'
import { useDispatch, useSelector } from 'react-redux'
import { passwordResetAction } from '../../Store/Auth/Actions/SignupAction'
import { VALUE_FAILED, VALUE_REQUESTED } from '../../Constants/ActionTypes'
import { showErrorToast } from '../../Utils/ShowToast'
import { FORGOT_PASSWORD_FORM } from '../../Constants/FormNames'

const ForgetPassword = props => {
  const { Gutters, Layout } = useTheme()
  const { handleSubmit } = props
  const signupState = useSelector(state => state.signup)
  const dispatch = useDispatch()
  const passwordResetStatus = signupState.passwordResetStatus

  useEffect(() => {
    if (passwordResetStatus === VALUE_FAILED) {
      showErrorToast({
        message: signupState.error,
      })
    }
  }, [passwordResetStatus, signupState.error])

  const handleForgetPasswordSubmit = values => {
    dispatch(passwordResetAction(values))
  }

  return (
    <Container>
      <LightHeader navigation={props.navigation} left={true} />
      <Content>
        <View style={[styles.padding20, styles.marginTop50]}>
          <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
            <Text style={styles.forgotPasswordTitle}>Forgot Password?</Text>
            <Text style={styles.forgotPasswordSubtitle}>
              Enter the email associated with your account and we’ll send you a
              link to reset your password.
            </Text>
          </View>
          <View style={styles.marginTop40}>
            <Field
              name="email"
              component={RenderInput}
              placeholder="Email"
              validate={[required, email]}
              label="Email"
            />
          </View>
          <View style={(styles.alignCenter, styles.marginTop20)}>
            <BojoButtonYellow
              navigation={props.navigation}
              loading={passwordResetStatus === VALUE_REQUESTED}
              title="Submit"
              onClick={handleSubmit(handleForgetPasswordSubmit)}
            />
          </View>
        </View>
      </Content>
    </Container>
  )
}

export default reduxForm({
  form: FORGOT_PASSWORD_FORM,
})(ForgetPassword)
