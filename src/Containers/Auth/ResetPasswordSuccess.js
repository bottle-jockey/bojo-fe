import React from 'react'
import { View, Text } from 'react-native'
import { useTheme } from '@/Theme'
import { Container, Content } from 'native-base'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import styles from '../../Assets/styles'
import { LOGIN_SCREEN } from '../../Constants/Routes'

const ResetPasswordSuccess = ({ navigation }) => {
  const { Gutters, Layout } = useTheme()

  return (
    <Container>
      <LightHeader navigation={navigation} />
      <Content>
        <View style={[styles.marginTop50, styles.padding20]}>
          <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
            <Text style={styles.forgotPasswordTitle}>Success!</Text>
            <Text style={styles.forgotPasswordSubtitle}>
              Your password has been reset.
            </Text>
          </View>
          <View style={[styles.marginTop15, styles.alignCenter]}>
            <BojoButtonYellow
              navigation={navigation}
              title="Back to Log in"
              onPress={() => navigation.navigate(LOGIN_SCREEN)}
            />
          </View>
        </View>
      </Content>
    </Container>
  )
}

export default ResetPasswordSuccess
