import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import { useTheme } from '@/Theme'
import { Container, Content } from 'native-base'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import RenderInput from '@/Components/Forms/RenderInput'
import { required, password, passwordsMustMatch } from '@/Utils/Validation'
import { Field, reduxForm } from 'redux-form'
import styles from '../../Assets/styles'
import { useDispatch, useSelector } from 'react-redux'
import { VALUE_FAILED, VALUE_REQUESTED } from '../../Constants/ActionTypes'
import { showErrorToast } from '../../Utils/ShowToast'
import { passwordResetConfirmAction } from '../../Store/Auth/Actions/SignupAction'
import { RESET_PASSWORD_FORM } from '@/Constants/FormNames'

const ResetPassword = ({ navigation, handleSubmit }) => {
  const { Gutters, Layout } = useTheme()

  const signupState = useSelector(state => state.signup)
  const dispatch = useDispatch()

  const pwdResetConfirmStatus = signupState.passwordResetConfirmStatus

  useEffect(() => {
    if (pwdResetConfirmStatus === VALUE_FAILED) {
      showErrorToast({
        message: signupState.error,
      })
    }
  }, [pwdResetConfirmStatus, signupState.error])

  const handleResetPasswordSubmit = values => {
    dispatch(
      passwordResetConfirmAction({
        ...values,
        token: '<PLACE_TOKEN_HERE>',
      }),
    )
  }

  return (
    <Container>
      <LightHeader navigation={navigation} />
      <Content>
        <View style={[styles.marginTop50, styles.padding20]}>
          <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
            <Text style={styles.forgotPasswordTitle}>Reset Password</Text>
            <Text style={styles.forgotPasswordSubtitle}>
              Enter the email associated with your account and we’ll send you a
              link to reset your password.
            </Text>
          </View>
          <View style={styles.marginTop40}>
            <Field
              name="password"
              component={RenderInput}
              placeholder="Password"
              password={true}
              label="Password"
              validate={[required, password]}
            />
          </View>
          <View style={styles.marginTop15}>
            <Field
              name="confirmResetPassword"
              component={RenderInput}
              placeholder="Confirm Password"
              password={true}
              label="Confirm Password"
              validate={[required, passwordsMustMatch]}
            />
          </View>
          <View style={[styles.marginTop15, styles.alignCenter]}>
            <BojoButtonYellow
              navigation={navigation}
              title="Reset"
              loading={pwdResetConfirmStatus === VALUE_REQUESTED}
              onClick={handleSubmit(handleResetPasswordSubmit)}
            />
          </View>
        </View>
      </Content>
    </Container>
  )
}

export default reduxForm({
  form: RESET_PASSWORD_FORM,
})(ResetPassword)
