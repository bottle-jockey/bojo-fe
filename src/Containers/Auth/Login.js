import React, { useEffect } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { useTheme } from '@/Theme'
import { Container, Content, Footer, Toast } from 'native-base'
import NameLogo from '@/Assets/Images/BojoNameIconLight.svg'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import FbSignin from '@/Assets/BojoInputs/FbSignin'
import GoogleSignin from '@/Assets/BojoInputs/GoogleSignin'
import { required, email } from '@/Utils/Validation'
import { Field, reduxForm } from 'redux-form'
import RenderInput from '@/Components/Forms/RenderInput'
import styles from '../../Assets/styles'
import { FORGET_PASSWORD_SCREEN, SIGNUP_SCREEN } from '../../Constants/Routes'
import { useDispatch, useSelector } from 'react-redux'
import { LOGIN_FAILED, LOGIN_REQUESTED } from '../../Constants/ActionTypes'
import { loginUserAction } from '../../Store/Auth/Actions/LoginAction'
import { useTranslation } from 'react-i18next'
import loginSlice from '../../Store/Auth/Login'

const Login = ({ navigation, handleSubmit }) => {
  const { t } = useTranslation()
  const { Gutters, Layout } = useTheme()

  const loginState = useSelector(state => state.login)
  const dispatch = useDispatch()

  useEffect(() => {
    return () => {
      dispatch(loginSlice.actions.LOGIN_RESET())
    }
  }, [dispatch])

  useEffect(() => {
    if (loginState.status === LOGIN_FAILED) {
      Toast.show({
        text: loginState.error,
        buttonText: t('actions.okay'),
        type: 'danger',
        duration: 10000,
        position: 'top',
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loginState.status, loginState.error])

  const handleLoginSubmitClick = values => {
    dispatch(loginUserAction(values))
  }

  return (
    <Container style={styles.loginContainer}>
      <Content>
        <View style={styles.alignCenter}>
          <NameLogo />
        </View>
        <View style={styles.marginTop40}>
          <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
            <Text style={styles.welcome}>Welcome!</Text>
            <Text style={styles.subtext}>
              Host a tasting event or join the party!
            </Text>
          </View>
          <View style={styles.marginTop40}>
            <Field
              name="email"
              component={RenderInput}
              placeholder={t('placeholders.email')}
              label="Email"
              validate={[required, email]}
            />
          </View>
          <View style={styles.marginTop20}>
            <Field
              name="password"
              component={RenderInput}
              placeholder={t('placeholders.password')}
              label="Password"
              password={true}
              validate={[required]}
            />
          </View>
          <View style={[styles.alignCenter, styles.marginTop20]}>
            <BojoButtonYellow
              navigation={navigation}
              title={t('actions.login')}
              loading={loginState.status === LOGIN_REQUESTED}
              onClick={handleSubmit(handleLoginSubmitClick)}
            />
          </View>
          <View style={[styles.alignCenter, styles.marginTop40]}>
            <FbSignin />
          </View>
          <View style={[styles.alignCenter, styles.marginTop20]}>
            <GoogleSignin />
          </View>
        </View>
      </Content>
      <Footer
        style={{
          ...styles.alignCenter,
          display: 'flex',
          flexDirection: 'column',
          backgroundColor: 'transparent',
          borderTopWidth: 0,
          elevation: 0, // FOR ANDROID
        }}
      >
        <TouchableOpacity
          onPress={() => navigation.navigate(FORGET_PASSWORD_SCREEN)}
        >
          <Text style={styles.forgotPassword}>
            {t('actions.forgotPassword')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate(SIGNUP_SCREEN)}>
          <Text style={styles.createAccount}>
            {t('actions.doNtHaveAccount')}
          </Text>
        </TouchableOpacity>
      </Footer>
    </Container>
  )
}

export default reduxForm({
  form: 'LoginDetails',
})(Login)
