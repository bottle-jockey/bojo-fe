import React from 'react'
import { View, Text } from 'react-native'
import { useTheme } from '@/Theme'
import { Container, Content } from 'native-base'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import styles from '../../Assets/styles'
import { LOGIN_SCREEN } from '../../Constants/Routes'

const ForgetPasswordLink = ({ navigation }) => {
  const { Gutters, Layout } = useTheme()

  return (
    <Container>
      <LightHeader navigation={navigation} left={true} />
      <Content>
        <View style={[styles.padding20, styles.marginTop50]}>
          <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
            <Text style={styles.forgotPasswordTitle}>Forgot Password?</Text>
            <Text style={styles.forgotPasswordSubtitle}>
              You should receive an email in a few moments. Please open the link
              to reset your password.
            </Text>
          </View>
          <View>
            <Text style={styles.forgotPasswordEmail}>abc@gmail.com</Text>
          </View>
          <View style={(styles.alignCenter, styles.marginTop20)}>
            <BojoButtonYellow
              navigation={navigation}
              title="Back to Log In"
              onPress={() => navigation.navigate(LOGIN_SCREEN)}
            />
          </View>
        </View>
      </Content>
    </Container>
  )
}

export default ForgetPasswordLink
