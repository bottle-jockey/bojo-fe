import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import { useTheme } from '@/Theme'
import { Container, Toast } from 'native-base'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import RenderInput from '@/Components/Forms/RenderInput'
import {
  required,
  email,
  password,
  passwordsMustMatch,
  phone,
} from '@/Utils/Validation'
import { Field, reduxForm } from 'redux-form'
import RenderTextInput from '@/Components/Forms/RenderTextInput'
import styles from '../../../Assets/styles'
import {
  BASIC_DETAIL_STEP,
  NAME_DETAIL_STEP,
} from '../../../Constants/FormNames'
import { useDispatch, useSelector } from 'react-redux'
import { signupAction } from '../../../Store/Auth/Actions/SignupAction'
import { SIGNUP_FAILED, SIGNUP_REQUESTED } from '../../../Constants/ActionTypes'
import { showErrorToast } from '@/Utils/ShowToast'

const ContactDetails = props => {
  const { Gutters, Layout } = useTheme()
  const { handleSubmit } = props

  const nameDetailsForm = useSelector(state => state.form[NAME_DETAIL_STEP])
  const signupState = useSelector(state => state.signup)
  const dispatch = useDispatch()

  useEffect(() => {
    if (signupState.status === SIGNUP_FAILED) {
      showErrorToast({
        message: signupState.error,
      })
    }
  }, [signupState.status, signupState.error])

  const handleNextButtonClick = values => {
    if (!nameDetailsForm) {
      return Toast.show({
        type: 'danger',
        text: 'Unable to proceed. Try re entering the form',
      })
    }
    const nameDetails = nameDetailsForm.values
    let phoneNumber = values.phone_number
    phoneNumber = phoneNumber.replace(' ', '')
    phoneNumber = phoneNumber.replace('(', '')
    phoneNumber = phoneNumber.replace(')', '')
    phoneNumber = phoneNumber.replace('-', '')

    const payload = {
      ...nameDetails,
      ...values,
      phone_number: phoneNumber,
    }

    dispatch(signupAction(payload))
  }
  const normalizePhone = value => {
    if (!value) {
      return value
    }

    const onlyNums = value.replace(/[^\d]/g, '')
    if (onlyNums.length <= 3) {
      return onlyNums
    }
    if (onlyNums.length <= 7) {
      return `${onlyNums.slice(0, 3)}-${onlyNums.slice(3)}`
    }
    return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(
      6,
    )}`
  }
  const phoneFormatter = number => {
    if (!number) {
      return ''
    }
    // NNN-NNN-NNNN
    const onlyNums = number.replace(/[^\d]/g, '')
    return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(
      6,
    )}`
  }

  return (
    <Container>
      <LightHeader navigation={props.navigation} left={true} />
      <View style={[styles.padding20, styles.marginTop30]}>
        <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
          <Text style={styles.signUpTitle}>Sign Up</Text>
          <Text style={styles.signUpSubtitle}>Let's create your account!</Text>
        </View>
        <View style={styles.marginTop40}>
          <Field
            name="email"
            component={RenderInput}
            placeholder="Email"
            validate={[required, email]}
            label="Email"
            keyboardType="email-address"
          />
        </View>
        <View style={styles.marginTop15}>
          <Field
            name="phone_number"
            component={RenderTextInput}
            placeholder="(000) 000 - 0000"
            normalize={normalizePhone}
            validate={[required]}
            label="Phone Number"
            format={phoneFormatter}
            keyboardType="phone-pad"
          />
        </View>
        <View style={styles.marginTop15}>
          <Field
            name="password"
            component={RenderInput}
            placeholder="Password"
            label="Password"
            password={true}
            validate={[required, password]}
          />
        </View>
        <View style={styles.marginTop15}>
          <Field
            name="confirmPassword"
            component={RenderInput}
            placeholder="Confirm Password"
            label="Confirm Password"
            password={true}
            validate={[required, passwordsMustMatch]}
          />
        </View>
      </View>
      <View style={styles.screenPositionBottom1}>
        <View style={styles.screenPositionBottom2}>
          <View>
            <BojoButtonYellow
              navigation={props.navigation}
              title="Next"
              loading={signupState.status === SIGNUP_REQUESTED}
              onClick={handleSubmit(handleNextButtonClick)}
            />
          </View>
        </View>
      </View>
    </Container>
  )
}

export default reduxForm({
  form: BASIC_DETAIL_STEP,
})(ContactDetails)
