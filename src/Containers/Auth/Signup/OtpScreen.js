/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { useTheme } from '@/Theme'
import { Container, Content } from 'native-base'
import BojoInputBox from '@/Assets/BojoInputs/BojoInputBox'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import { useDispatch, useSelector } from 'react-redux'
import {
  OTP_VERIFY_FAILED,
  OTP_VERIFY_REQUESTED,
} from '../../../Constants/ActionTypes'
import { showErrorToast } from '@/Utils/ShowToast'
import { otpVerifyAction } from '../../../Store/Auth/Actions/SignupAction'
import styles from '../../../Assets/styles'

const OtpScreen = ({ navigation }) => {
  const { Gutters, Layout } = useTheme()
  const [otp, setOtp] = useState('')

  const signupState = useSelector(state => state.signup)
  const otpVerifyStatus = signupState?.otpVerifyStatus
  const user = signupState.data.user
  const dispatch = useDispatch()

  useEffect(() => {
    if (otpVerifyStatus === OTP_VERIFY_FAILED) {
      showErrorToast({
        message: signupState.error,
      })
    }
  }, [otpVerifyStatus, signupState.error])

  const handleOTPSubmit = () => {
    if (!user) {
      return showErrorToast({
        message: 'User object is not defined',
      })
    }

    dispatch(otpVerifyAction({ otp, phone_number: user.phone_number }))
  }

  const renderPhoneText = () => {
    let phoneText = 'XXXX'
    if (user) {
      phoneText = user.phone_number.substring(6)
    }

    return (
      <Text
        style={{
          color: '#29121D',
          fontFamily: 'CalibriRegular',
          textAlign: 'center',
          marginTop: 10,
          fontSize: 14,
        }}
      >
        {`Please enter the OTP sent to (XXX) XXX- ${phoneText}`}
      </Text>
    )
  }

  return (
    <Container style={{}}>
      <LightHeader navigation={navigation} left={true} />
      <Content>
        <View style={{ padding: 20, marginTop: 50 }}>
          <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
            <Text
              style={{
                color: '#29121D',
                fontFamily: 'FreigBigProBol',
                fontSize: 44,
              }}
            >
              Sign Up
            </Text>
            {renderPhoneText()}
          </View>
          <View style={{ alignItems: 'center', marginTop: 20 }}>
            <BojoInputBox
              placeholder="OTP"
              value={otp}
              onChangeText={val => setOtp(val)}
            />
          </View>
          <View style={{ alignItems: 'center', marginTop: 15 }}>
            <BojoButtonYellow
              navigation={navigation}
              title="Submit"
              loading={otpVerifyStatus === OTP_VERIFY_REQUESTED}
              onClick={() => handleOTPSubmit()}
            />
          </View>
          <View style={[styles.alignCenter, styles.marginTop30]}>
            <TouchableOpacity onPress={() => {}}>
              <Text style={[styles.fontWeightBold]}> Resend?</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Content>
    </Container>
  )
}

export default OtpScreen
