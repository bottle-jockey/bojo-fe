import React, { useState } from 'react'
import { View, Text } from 'react-native'
import { useTheme } from '@/Theme'
import { Container } from 'native-base'
import BojoOutlineBlack from '@/Assets/BojoInputs/BojoOutlineBlack'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import { Field, reduxForm } from 'redux-form'
import Modal from 'react-native-modal'
import RenderInput from '@/Components/Forms/RenderInput'
import { required } from '@/Utils/Validation'
import { AUTH_CONTACT_SCREEN, LOGIN_SCREEN } from '../../../Constants/Routes'
import styles from '../../../Assets/styles'
import { NAME_DETAIL_STEP } from '../../../Constants/FormNames'

const NameDetails = props => {
  const { Gutters, Layout } = useTheme()
  const [ageModal, setAgeModal] = useState(true)
  const [belowAgeModal, setBelowAgeModal] = useState(false)
  const { handleSubmit } = props

  const handleBelowAgeModalClick = () => {
    setAgeModal(false)
    setBelowAgeModal(false)
    props.navigation.navigate(LOGIN_SCREEN)
  }

  const handleNextButtonClick = values => {
    props.navigation.navigate(AUTH_CONTACT_SCREEN)
  }

  return (
    <Container>
      <LightHeader navigation={props.navigation} left={true} />
      <View style={styles.marginTop50}>
        <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
          <Text style={styles.signUpTitle}>Sign Up</Text>
          <Text style={styles.signUpSubtitle}>Let's create your account!</Text>
        </View>
        <View style={styles.borderRadius10}>
          <Modal isVisible={ageModal} style={styles.borderRadius10}>
            <View style={styles.modal1}>
              <Text style={[styles.modal1Text, styles.fontWeightBold]}>
                Are you 21 or older?
              </Text>
              <View style={styles.modal1buttonRow}>
                <View style={styles.modal1button}>
                  <BojoOutlineBlack
                    navigation={props.navigation}
                    title="No"
                    onClick={() => setBelowAgeModal(true)}
                    style={styles.bojoOutlineBlackButton}
                    bordered={true}
                  />
                </View>
                <View style={styles.modal1button}>
                  <BojoButtonYellow
                    navigation={props.navigation}
                    title="Yes"
                    onClick={() => setAgeModal(false)}
                  />
                </View>
              </View>
            </View>
          </Modal>
        </View>
        <View>
          <Modal isVisible={belowAgeModal}>
            <View style={styles.modal2}>
              <Text style={styles.modal2Title}>Hold on!</Text>
              <Text style={styles.modal2Subtitle}>
                You must be at least 21 years old to access the BottleJockey
                app.
              </Text>
              <View style={styles.marginTop30}>
                <BojoButtonYellow
                  navigation={props.navigation}
                  title="Okay"
                  onClick={handleBelowAgeModalClick}
                />
              </View>
            </View>
          </Modal>
        </View>
        <View style={styles.marginTop30}>
          <Field
            name="first_name"
            component={RenderInput}
            placeholder="First Name"
            validate={[required]}
            label="First Name"
          />
        </View>
        <View style={styles.marginTop15}>
          <Field
            name="last_name"
            component={RenderInput}
            placeholder="Last Name"
            label="Last Name"
            validate={[required]}
          />
        </View>
      </View>
      <View style={styles.screenPositionBottom1}>
        <View style={styles.screenPositionBottom2}>
          <View>
            <BojoButtonYellow
              navigation={props.navigation}
              title="Next"
              onClick={handleSubmit(handleNextButtonClick)}
            />
          </View>
        </View>
      </View>
    </Container>
  )
}

export default reduxForm({
  form: NAME_DETAIL_STEP,
})(NameDetails)
