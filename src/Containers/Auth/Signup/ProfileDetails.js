/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { useTheme } from '@/Theme'
import { Container, Button, Icon } from 'native-base'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import { Field, reduxForm } from 'redux-form'
import RenderTextInput from '@/Components/Forms/RenderTextInput'
import DropDownPicker from 'react-native-dropdown-picker'
import Modal from 'react-native-modal'
import DocumentPicker from 'react-native-document-picker'
import { pickDocument } from '@/Utils/FileUtils'
import { jsonToFormData } from '@/Utils/CommonUtils'
import { showErrorToast } from '@/Utils/ShowToast'
import RNFS from 'react-native-fs'
import CameraModal from '@/Utils/CameraModal'
import styles from '../../../Assets/styles'
import { useDispatch, useSelector } from 'react-redux'
import { profileUpdateAction } from '../../../Store/Auth/Actions/SignupAction'
import { VALUE_FAILED, VALUE_REQUESTED } from '../../../Constants/ActionTypes'
import { HOME_SCREEN } from '../../../Constants/Routes'

const ProfileDetails = ({ navigation, handleSubmit }) => {
  const { Gutters, Layout } = useTheme()

  const signupState = useSelector(state => state.signup)
  const authState = useSelector(state => state.auth)
  const dispatch = useDispatch()

  const [open, setOpen] = useState(false)
  const [value, setValue] = useState(authState.data.gender || null)
  const [items, setItems] = useState([
    { label: 'Male', value: 'male' },
    { label: 'Female', value: 'female' },
    { label: 'Others', value: 'others' },
    { label: 'Prefer Not to Say', value: 'prefer' },
  ])
  const [uploadModal, setUploadModal] = useState(false)
  const [cameraOpen, setCameraOpen] = useState(false)
  const [imageUrl, setImageUrl] = useState('')

  const profileStatus = signupState.profileStatus

  useEffect(() => {
    if (profileStatus === VALUE_FAILED) {
      showErrorToast({
        message: signupState.error,
      })
    }
  }, [profileStatus, signupState.error])

  const dateFormatter = number => {
    if (!number) {
      return ''
    }
    // MM/DD/YYYY
    const onlyNums = number.replace(/[^\d]/g, '')
    const month = onlyNums.slice(0, 2)
    const date = onlyNums.slice(2, 4)
    return `${month <= 12 ? month : '12'}${onlyNums.length > 2 ? '/' : ''}${
      date <= 31 ? date : '31'
    }${onlyNums.length > 4 ? '/' : ''}${onlyNums.slice(4, 8)}`
  }

  const renderInputButton = () => (
    <Button
      bordered
      onPress={() => setUploadModal(true)}
      style={styles.uploadButton}
    >
      <Icon style={{ color: '#d7d4cd' }} name="camera" />
      <Text style={{ color: '#d7d4cd', flex: 0.8 }}>+ Upload a photo</Text>
    </Button>
  )

  const uploadImage = () => {
    pickDocument({ type: [DocumentPicker.types.images] })
      .then(result => {
        setImageUrl(result.uri)
        convertBase64(result)
      })
      .catch(err => console.log('error:', err))
  }
  const turnOffCamera = () => setCameraOpen(false)
  const handleImageCapture = photo => console.log(photo)

  const handleProfileSubmit = values => {
    const payload = {
      ...values,
      gender: value,
    }

    const formData = jsonToFormData(payload)
    dispatch(profileUpdateAction(formData))
  }

  const renderCamera = () => {
    return (
      <CameraModal
        show={cameraOpen}
        onClose={turnOffCamera}
        onSave={handleImageCapture}
      />
    )
  }

  const convertBase64 = image => {
    RNFS.readFile(image.fileCopyUri, 'base64').then(res => {
      setUploadModal(false)
    })
  }

  const renderActions = () => {
    return (
      <Button
        transparent
        large
        onPress={() => navigation.navigate(HOME_SCREEN)}
      >
        <Text style={{ fontFamily: 'CalibriBold', fontWeight: 'bold' }}>
          Skip
        </Text>
      </Button>
    )
  }

  return (
    <>
      <Container>
        <LightHeader navigation={navigation} actions={renderActions()} />
        <View style={[styles.padding20, styles.marginTop40]}>
          <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
            <Text style={styles.signUpTitle}>Welcome, Eric!</Text>
          </View>
          <View style={[styles.alignCenter, styles.marginTop15]}>
            <Text style={styles.label}>
              Want to receive special offers on your birthday?
            </Text>
            <Field
              name="date_of_birth"
              component={RenderTextInput}
              placeholder="MM/DD/YYYY"
              placeholderTextColor="#d7d4cd"
              label="Date of Birth"
              format={dateFormatter}
              keyboardType="numeric"
              onChange={() => setOpen(false)}
            />
          </View>
          <View style={styles.marginTop15}>
            <Text style={styles.label}>Gender</Text>
            <DropDownPicker
              open={open}
              value={value}
              items={items}
              setOpen={setOpen}
              setValue={setValue}
              setItems={setItems}
              placeholder="Select"
              placeholderStyle={{ color: '#D7D4CD' }}
              style={{ borderColor: '#D7D4CD' }}
              textStyle={{
                textAlign: 'center',
                fontSize: 12,
              }}
              closeAfterSelecting={true}
              itemSeparator={true}
              itemSeparatorStyle={{ backgroundColor: '#d7d4cd' }}
              showTickIcon={false}
              selectedItemContainerStyle={{
                backgroundColor: '#d7d4cd',
              }}
              listItemContainerStyle={{
                height: 50,
              }}
              dropDownContainerStyle={{
                backgroundColor: '#fff',
                borderColor: '#D7D4CD',
              }}
              arrowIconStyle={{
                width: 15,
                height: 15,
                marginRight: 0,
              }}
            />
          </View>
          <View style={styles.marginTop15}>
            <View style={styles.alignCenter}>
              <Text style={styles.label}>Profile Picture</Text>
            </View>
            {renderInputButton()}
          </View>
        </View>
        <View style={styles.screenPositionBottom1}>
          <View style={styles.screenPositionBottom2}>
            <View>
              <BojoButtonYellow
                navigation={navigation}
                title="Next"
                loading={profileStatus === VALUE_REQUESTED}
                onClick={handleSubmit(handleProfileSubmit)}
              />
            </View>
          </View>
        </View>
      </Container>
      <View>
        <Modal
          isVisible={uploadModal}
          avoidKeyboard={true}
          onBackdropPress={() => setUploadModal(false)}
          style={{ margin: 0 }}
        >
          <View style={styles.uploadModal}>
            <TouchableOpacity onPress={() => setCameraOpen(true)}>
              <View style={{ flexDirection: 'row' }}>
                <Icon style={{ color: '#29121D' }} name="camera" />
                <Text style={styles.uploadModalText}>Take Photo</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={uploadImage} style={styles.marginTop15}>
              <View style={{ flexDirection: 'row' }}>
                <Icon style={{ color: '#29121D' }} name="add-circle-outline" />
                <Text style={styles.uploadModalText}>
                  Upload from Camera Roll
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>
        {cameraOpen && renderCamera()}
      </View>
    </>
  )
}

export default reduxForm({
  form: 'ProfileDetails',
})(ProfileDetails)
