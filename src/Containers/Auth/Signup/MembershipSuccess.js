/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { useTheme } from '@/Theme'
import {
  Container,
  Button,
  Icon,
  Header,
  Body,
  Left,
  Right,
  Content,
} from 'native-base'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import styles from '../../../Assets/styles'
import { HOME_SCREEN } from '../../../Constants/Routes'
import BojoLogo from '@/Assets/Images/BojoLogo.svg'
import { useTranslation } from 'react-i18next'

const PremiumMemberShipSuccess = ({ navigation }) => {
  const { t } = useTranslation()

  return (
    <Container style={[styles.bojoBackground, styles.padding20]}>
      <View style={[styles.alignCenter]}>
        <BojoLogo height={60} />
        <Text style={[styles.membershipText, styles.marginTop30]}>
          {t('membership.success')}
        </Text>
        <Text
          style={[
            styles.alignCenter,
            styles.marginTop20,
            styles.fontColorWhite,
          ]}
        >
          {t('membership.accessPremium')}
        </Text>
        <Text style={[styles.alignCenter, styles.fontColorWhite]}>
          {t('membership.memberId')}
        </Text>
        <Text
          style={[styles.alignCenter, styles.marginTop20, styles.membershipBg]}
        >
          XXXX
        </Text>
      </View>
      <View
        style={[
          styles.padding30,
          styles.borderRadius10,
          styles.marginTop30,
          styles.alignCenter,
        ]}
      >
        <Text style={styles.fontColorWhite}>{t('membership.email')}</Text>
        <Text style={styles.fontColorYellow}>eric@email.com </Text>
      </View>
      <View style={styles.screenPositionBottom1}>
        <View style={styles.screenPositionBottom2}>
          <BojoButtonYellow
            title={t('membership.finish')}
            onClick={() => navigation.navigate(HOME_SCREEN)}
          />
        </View>
      </View>
    </Container>
  )
}

export default PremiumMemberShipSuccess
