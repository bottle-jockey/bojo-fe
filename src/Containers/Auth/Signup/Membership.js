/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { useTheme } from '@/Theme'
import {
  Container,
  Button,
  Icon,
  Header,
  Body,
  Left,
  Right,
  Content,
} from 'native-base'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import LightHeader from '@/Assets/BojoInputs/LightHeader'
import { Field, reduxForm } from 'redux-form'
import RenderTextInput from '@/Components/Forms/RenderTextInput'
import DropDownPicker from 'react-native-dropdown-picker'
import Modal from 'react-native-modal'
import DocumentPicker from 'react-native-document-picker'
import { pickDocument } from '@/Utils/FileUtils'
import RNFS from 'react-native-fs'
import CameraModal from '@/Utils/CameraModal'
import styles from '../../../Assets/styles'
import { MEMBERSHIP_SUCCESS_SCREEN, HOME_SCREEN } from '../../../Constants/Routes'
import BojoLogo from '@/Assets/Images/BojoLogo.svg'
import {useTranslation} from 'react-i18next'

const PremiumMemberShipContainer = ({ navigation }) => {
	const { t } = useTranslation()

  const renderRow = data => {
    return (
      <View style={{ flexDirection: 'row' }}>
        <Text>{'\u2022'}</Text>
        <Text style={{ flex: 1, paddingLeft: 10 }}>{data}</Text>
      </View>
    )
  }
  return (
    <Container style={styles.bojoBackground}>
      <Content style={[styles.padding20, styles.marginTop30]}>
        <View style={[styles.alignCenter]}>
          <BojoLogo height={60} />
          <Text style={[styles.membershipText, styles.marginTop30]}>
            {t('membership.title')}
          </Text>
          <Text
            style={[
              styles.alignCenter,
              styles.marginTop20,
              styles.fontColorWhite,
            ]}
          >
            {t('membership.subtitle')}
          </Text>
          <Text
            style={[
              styles.alignCenter,
              styles.marginTop20,
              styles.membershipBg,
            ]}
          >
            {t('membership.badge')}
          </Text>
        </View>
        <View
          style={[
            styles.padding30,
            styles.borderRadius10,
						styles.marginTop30,
            { backgroundColor: '#fff' },
          ]}
        >
          <Text style={styles.fontWeightBold}>{t('membership.premium')}</Text>
          <View style={styles.marginTop20}>
            {renderRow(t('membership.hostEvents'))}
          </View>
          <View style={styles.marginTop10}>
            {renderRow(t('membership.accessToTastingNotes'))}
          </View>
          <View style={styles.marginTop10}>
            {renderRow(t('membership.abilityToSetNotes'))}
          </View>
          <View style={styles.marginTop10}>
            {renderRow(t('membership.limitedOffer'))}
          </View>
        </View>
        <View style={styles.marginTop30}>
          <BojoButtonYellow
            title={t('membership.premiumUpgrade')}
            onClick={() => navigation.navigate(MEMBERSHIP_SUCCESS_SCREEN)}
            style={{ height: 90 }}
          />
          <TouchableOpacity
            onPress={() => navigation.navigate(HOME_SCREEN)}
            style={[styles.alignCenter, styles.marginTop30]}
          >
            <Text style={[styles.fontColorWhite, styles.fontWeightBold]}>
              {t('membership.maybeLater')}
            </Text>
          </TouchableOpacity>
        </View>
      </Content>
    </Container>
  )
}

export default PremiumMemberShipContainer
