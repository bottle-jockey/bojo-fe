import React, { useState } from 'react'
import { Text, TouchableOpacity, TextInput } from 'react-native'
import {
  View,
  Header,
  Icon,
  Button,
  Container,
  Left,
  Right,
  Title,
  Content,
  Form,
  Textarea,
} from 'native-base'
import styles from '@/Assets/styles'
import { useTranslation } from 'react-i18next'
import BojoRibbon from '@/Assets/Images/win.svg'
import CheckBox from '@react-native-community/checkbox'
import RenderInput from '@/Components/Forms/RenderInput'
import {
  required,
  email,
  phone,
} from '@/Utils/Validation'
import { Field, reduxForm } from 'redux-form'
import RenderTextInput from '@/Components/Forms/RenderTextInput'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import AddFriend from '@/Assets/Images/add-friend.svg'
import Modal from 'react-native-modal'
import { Colors } from '../../../Theme/Variables'
import { Config } from '@/Config'

const EventDetailsScreen = props => {
  const { t } = useTranslation()
  const [contactInfo, setContactInfo] = useState(false)
  const [location, setLocation] = useState({
    name: '',
    geometry: {},
  })
  const currentDate = new Date()
  const [date, setDate] = useState(currentDate)
  const [formattedDate, setFormattedDate] = useState(
    moment(currentDate).format('L'),
  )
  const [show, setShow] = useState(false)
  const [time, setTime] = useState(currentDate)
  const [formattedTime, setFormattedTime] = useState(
    moment(currentDate).format('kk:mm'),
  )
  const [showTime, setShowTime] = useState(false)
  const [details, setDetails] = useState('')
  const [title, setTitle] = useState('Name your event')
  const [changeTitle, setChangeTitle] = useState(false)
  const [helpModal, setHelpModal] = useState(false)

  const normalizePhone = value => {
    if (!value) {
      return value
    }

    const onlyNums = value.replace(/[^\d]/g, '')
    if (onlyNums.length <= 3) {
      return onlyNums
    }
    if (onlyNums.length <= 7) {
      return `${onlyNums.slice(0, 3)}-${onlyNums.slice(3)}`
    }
    return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(
      6,
    )}`
  }
  const phoneFormatter = number => {
    if (!number) {
      return ''
    }
    // NNN-NNN-NNNN
    const onlyNums = number.replace(/[^\d]/g, '')
    return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(
      6,
    )}`
  }

  const renderLocationInput = () => {
    return (
      <GooglePlacesAutocomplete
        placeholder="Search"
        minLength={2}
        autoFocus={false}
        returnKeyType={'default'}
        fetchDetails={true}
        onPress={(data, details = null) => {
          // 'details' is provided when fetchDetails = true
          setLocation({
            name: data.description,
            geometry: details.geometry.location,
          })
        }}
        query={{
          key: 'AIzaSyDgevI3COMk94pXbGUxuwXl4Zf9smyYNmA',
          language: 'en',
          components: 'country:us',
        }}
        autoFillOnNotFound={true}
        onFail={error => console.log('error', error)}
        styles={{
          textInput: styles.bordered,
          listView: {
            borderWidth: 1,
            borderTopColor: 'transparent',
            borderColor: '#D7D4CD',
            marginTop: -5,
          },
        }}
      />
    )
  }

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || date
    setDate(currentDate)
    setFormattedDate(moment(currentDate).format('L'))
    setShow(false)
  }
  const onChangeTime = (event, selectedTime) => {
    const currentTime = selectedTime || time
    setTime(currentTime)
    setFormattedTime(moment(currentTime).format('kk:mm'))
    setShowTime(false)
  }

  return (
    <Container>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
          <Button transparent large onPress={() => props.navigation.goBack()}>
            <Icon name="arrow-back" style={{ color: '#fff' }} />
          </Button>
        </Left>
        <View style={{ justifyContent: 'center' }}>
          {!changeTitle ? (
            <Title style={styles.headerTitle}>
              {title}
              <TouchableOpacity
                onPress={() => {
                  setChangeTitle(true)
                }}
              >
                <Icon
                  name="pencil"
                  type="EvilIcons"
                  style={[styles.marginLeft5, styles.fontColorWhite]}
                />
              </TouchableOpacity>
            </Title>
          ) : (
            <View style={{ backgroundColor: 'white' }}>
              <TextInput
                autoFocus
                onChangeText={value => setTitle(value)}
                value={title}
                onBlur={() => setChangeTitle(false)}
              />
            </View>
          )}
        </View>
        <Right />
      </Header>
      <Content>
        <View style={[styles.padding20]}>
          <Text style={[styles.fontWeightBold, { textAlign: 'center' }]}>
            Please enter the event details to create an invite link:
          </Text>
          <View style={{ width: '100%', marginTop: 20 }}>
            <View style={styles.eventDetailsCard}>
              <View style={styles.marginTop05}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                  }}
                >
                  <View>
                    <BojoRibbon style={{ marginTop: -40 }} />
                  </View>
                  <View style={{ marginRight: 'auto', marginLeft: 20 }}>
                    <View>
                      <Text style={styles.fontSize18}>
                        Up to{' '}
                        <Text style={styles.fontWeightBold}>9 Attendees</Text>
                      </Text>
                      <Text style={[styles.fontSize9, styles.marginTop05]}>
                        9 bottles of Red Wine
                      </Text>
                    </View>
                  </View>
                  <View>
                    <TouchableOpacity
                      style={[
                        styles.marginLeft5,
                        styles.bojoBackground,
                        styles.padding15,
                        styles.borderRadius10,
                      ]}
                      onPress={() => console.log('aa')}
                    >
                      <AddFriend />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={[styles.marginTop20, styles.divider]} />
          <View style={[styles.marginTop20, styles.flexDirectionRow]}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <Text
                style={[
                  styles.fontWeightBold,
                  styles.fontColorBrown,
                  styles.marginTop05,
                ]}
              >
                Include contact information for this event
              </Text>
              <TouchableOpacity onPress={() => setHelpModal(true)}>
                <Icon
                  name="help-circle-outline"
                  type="Ionicons"
                  style={{ fontSize: 20, marginTop: 5 }}
                />
              </TouchableOpacity>
              <View>
                <Modal
                  isVisible={helpModal}
                  onBackdropPress={() => setHelpModal(false)}
                >
                  <View
                    style={[
                      styles.padding30,
                      styles.borderRadius10,
                      { backgroundColor: '#fff' },
                    ]}
                  >
                    <Text style={{ textAlign: 'center' }}>
                      Providing contact information will allow people you invite
                      to contact you with any questions regarding your event.
                    </Text>
                  </View>
                </Modal>
              </View>
            </View>
            <View>
              <CheckBox
                disabled={false}
                value={contactInfo}
                boxType={'square'}
                hideBox={false}
                tintColors={{ true: Colors.primaryYellow }}
                onCheckColor={'#29121D'}
                onValueChange={newValue => {
                  setContactInfo(newValue)
                }}
              />
            </View>
          </View>
          <View>
            {contactInfo && (
              <View>
                <Text style={styles.fontSize9}>
                  Please enter your email, phone number, or both.
                </Text>
                <View style={styles.marginTop20}>
                  <Field
                    name="email"
                    component={RenderInput}
                    placeholder="example@email.com"
                    validate={[required, email]}
                    label="Email"
                    keyboardType="email-address"
                    styles={{ textAlign: 'left' }}
                  />
                </View>
                <View style={styles.marginTop15}>
                  <Field
                    name="phone_number"
                    component={RenderTextInput}
                    placeholder="(000) 000 - 0000"
                    normalize={normalizePhone}
                    validate={[required]}
                    label="Phone Number"
                    format={phoneFormatter}
                    keyboardType="phone-pad"
                    style={{ textAlign: 'left' }}
                  />
                </View>
              </View>
            )}
          </View>
          <View style={[styles.marginTop20, styles.divider]} />
          <View style={[styles.marginTop20, styles.flexDirectionRow]}>
            <View style={styles.flexHalf}>
              <Text style={styles.fontWeightBold}>Date</Text>
              <TouchableOpacity
                style={[styles.bordered, styles.padding15]}
                onPress={() => setShow(true)}
              >
                <Text>{formattedDate}</Text>
              </TouchableOpacity>
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  minimumDate={new Date()}
                  display="default"
                  onChange={onChangeDate}
                />
              )}
            </View>
            <View style={[styles.flexHalf, styles.marginLeft5]}>
              <Text style={styles.fontWeightBold}>Time</Text>
              <TouchableOpacity
                style={[styles.bordered, styles.padding15]}
                onPress={() => setShowTime(true)}
              >
                <Text>{formattedTime}</Text>
              </TouchableOpacity>
              {showTime && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={time}
                  mode={'time'}
                  is24Hour={true}
                  minimumDate={new Date()}
                  display="default"
                  onChange={onChangeTime}
                />
              )}
            </View>
          </View>
          <View style={[styles.marginTop20]}>
            <View>
              <Text style={styles.fontWeightBold}>Location</Text>
              {renderLocationInput()}
            </View>
            <View style={[styles.marginTop20]}>
              <View>
                <Text style={styles.fontWeightBold}>Details</Text>
                <Form>
                  <Textarea
                    rowSpan={5}
                    bordered
                    style={[styles.borderRadius10]}
                    placeholder="Everyone please bring a bottle…"
                    placeholderTextColor={'#D7D4CD'}
                    onChangeText={val => setDetails(val)}
                  />
                </Form>
              </View>
            </View>
          </View>
          <View
            style={[styles.screenPositionBottom1, { paddingHorizontal: 0 }]}
          >
            <View style={styles.screenPositionBottom2}>
              <View>
                <BojoButtonYellow
                  navigation={props.navigation}
                  title="Next"
                  // loading={signupState.status === SIGNUP_REQUESTED}
                  // onClick={handleSubmit(handleNextButtonClick)}
                />
              </View>
            </View>
          </View>
        </View>
      </Content>
    </Container>
  )
}
export default reduxForm({
  form: 'Event Details',
})(EventDetailsScreen)
