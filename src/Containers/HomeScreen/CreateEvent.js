import React, { useState, useEffect } from 'react'
import { Text, TouchableOpacity, BackHandler, Alert } from 'react-native'
import {
  View,
  Header,
  Icon,
  Button,
  Container,
  Left,
  Right,
  Title,
} from 'native-base'
import styles from '../../Assets/styles'
import { useTranslation } from 'react-i18next'
import CheckBox from '@react-native-community/checkbox'
import DropDownPicker from 'react-native-dropdown-picker'
import EmptyBottle from '@/Assets/Images/emptyBottle.svg'
import FullBottle from '@/Assets/Images/fullBottle.svg'
import Slider from '@react-native-community/slider'
import BojoButtonYellow from '@/Assets/BojoInputs/BojoButtonYellow'
import Modal from 'react-native-modal'
import { Colors } from '@/Theme/Variables'
import {
  EVENT_DETAILS_SCREEN
} from '../../Constants/Routes'

const CreateEventScreen = props => {
  const { t } = useTranslation()
  const [usersOpen, setUsersOpen] = useState(false)
  const [usersCount, setUsersCount] = useState(null)
  const [userLists, setUserLists] = useState([
    { label: '1', value: '1' },
    { label: '2', value: '2' },
    { label: '3', value: '3' },
    { label: '4', value: '4' },
    { label: '5', value: '5' },
    { label: '6', value: '6' },
    { label: '7', value: '7' },
    { label: '8', value: '8' },
    { label: '9', value: '9' },
  ])
  const [userAsTaster, setUserAsTaster] = useState(false)
  const [open, setOpen] = useState(false)
  const [wineType, setWineType] = useState(null)
  const [wineLists, setWineLists] = useState([
    { label: t('createEvent.redWine'), value: 'RED_WINE' },
    { label: t('createEvent.whiteWine'), value: 'WHITE_WINE' },
  ])
  const [bringBottles, setBringBottles] = useState(false)
  const [bottleCount, setBottleCount] = useState(1)
  var [emptyBottle, setEmptyBottle] = useState([
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ])
  const [error, setError] = useState(false)
  const [helpModal, setHelpModal] = useState(false)
  const usersCountChanged = Boolean(usersCount)
  const wineTypeChanged = Boolean(wineType)
  const userAsTasterChanged = Boolean(userAsTaster)
  const bringBottlesChanged = Boolean(bringBottles)
  const [bottleCountChanged, setBottleCountChanged] = useState(false)
  const hasUnsavedChanges =
    usersCountChanged ||
    wineTypeChanged ||
    userAsTasterChanged ||
    bringBottlesChanged ||
    bottleCountChanged

  const handleNextButtonClick = () => {
    usersCount !== 0 && wineType !== null
      ? props.navigation.navigate(EVENT_DETAILS_SCREEN)
      : setError(true)
  }

  // ComponentDidUpdate
  useEffect(() => {
    usersCount !== 0 && wineType !== null && setError(false)
  }, [usersCount, wineType])

  useEffect(
    () =>
      props.navigation.addListener('beforeRemove', e => {
        const action = e.data.action
        if (!hasUnsavedChanges) {
          return
        }

        e.preventDefault()

        Alert.alert(
          'Discard changes?',
          'You have unsaved changes. Are you sure to discard them and leave the screen?',
          [
            { text: "Don't leave", style: 'cancel', onPress: () => {} },
            {
              text: 'Discard',
              style: 'destructive',
              onPress: () => props.navigation.dispatch(action),
            },
          ],
        )
      }),
    [hasUnsavedChanges, props.navigation],
  )

  const usersCountIndex =
    usersCount !== null
      ? userLists.findIndex(user => user.label === usersCount)
      : 0

  const handleBottleClick = value => {
    if (bottleCount > value) {
      [...Array(bottleCount - value)].forEach((element, i) => {
        emptyBottle.push(false)
      })
    } else {
      [...Array(value - bottleCount)].forEach((element, i) => {
        emptyBottle.pop()
      })
    }
  }

  return (
    <Container>
      <Header style={styles.header}>
        <Left>
          <Button transparent large onPress={() => props.navigation.goBack()}>
            <Icon name="arrow-back" style={{ color: '#fff' }} />
          </Button>
        </Left>
        <View style={{ justifyContent: 'center', marginLeft: 60 }}>
          <Title style={styles.headerTitle}>
            {t('createEvent.createEvent')}
          </Title>
        </View>
        <Right>
          <Button transparent large onPress={() => props.navigation.goBack()}>
            <Icon
              name="information-outline"
              type="MaterialCommunityIcons"
              style={{ color: '#fff' }}
            />
          </Button>
        </Right>
      </Header>
      <View style={[styles.padding20]}>
        {error && (
          <View style={[styles.alignCenter, { marginBottom: 15 }]}>
            <Text style={[styles.errorText]}>
              {' '}
              Please complete all required fields.{' '}
            </Text>
          </View>
        )}
        <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>
          {t('createEvent.peopleCount')}
        </Text>
        <View style={styles.marginTop10}>
          <DropDownPicker
            open={usersOpen}
            value={usersCount}
            items={userLists}
            setOpen={() => {
              setUsersOpen(!usersOpen)
              setOpen(false)
            }}
            setValue={setUsersCount}
            setItems={setUserLists}
            placeholder="Select"
            placeholderStyle={{ color: '#D7D4CD' }}
            style={{
              borderColor: error && usersCount === null ? Colors.error : '#D7D4CD',
            }}
            closeAfterSelecting={true}
            itemSeparator={true}
            itemSeparatorStyle={{ backgroundColor: '#d7d4cd' }}
            showTickIcon={false}
            selectedItemContainerStyle={{
              backgroundColor: '#d7d4cd',
            }}
            textStyle={{ textAlign: 'center' }}
            listItemContainerStyle={{
              height: 40,
            }}
            dropDownContainerStyle={{
              backgroundColor: '#fff',
              borderColor: '#D7D4CD',
              height: 165,
            }}
            arrowIconStyle={{
              width: 15,
              height: 15,
              marginRight: 0,
            }}
          />
        </View>
        <View style={[styles.marginTop10, styles.flexDirectionRow]}>
          <Text style={styles.marginTop10}>
            {t('createEvent.inviteMorePeople')}
          </Text>
          <View
            style={{
              backgroundColor: Colors.primaryYellow,
              paddingVertical: 10,
              paddingHorizontal: 25,
              marginLeft: 15,
            }}
          >
            <TouchableOpacity>
              <Text style={[styles.fontWeightBold, styles.borderBottom]}>
                {t('createEvent.premiumUpgrade')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={[styles.marginTop20, styles.divider]} />
        <View style={[styles.marginTop15, styles.flexDirectionRow]}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Text
              style={[
                styles.fontWeightBold,
                styles.fontColorBrown,
                styles.marginTop05,
              ]}
            >
              {t('createEvent.userAsTaster')}
            </Text>
            <TouchableOpacity onPress={() => setHelpModal(true)}>
              <Icon
                name="help-circle-outline"
                type="Ionicons"
                style={{ fontSize: 20, marginTop: 5 }}
              />
            </TouchableOpacity>
          </View>
          <View>
            <CheckBox
              disabled={false}
              value={userAsTaster}
              boxType={'square'}
              hideBox={false}
              tintColors={{ true: Colors.primaryYellow }}
              onCheckColor={'#29121D'}
              onValueChange={newValue => {
                setUserAsTaster(newValue)
                newValue
                  ? setUsersCount(userLists[usersCountIndex + 1].value)
                  : usersCountIndex !== 0
                  ? setUsersCount(userLists[usersCountIndex - 1].value)
                  : setUsersCount(null)
              }}
            />
          </View>
        </View>
        <View style={[styles.marginTop20, styles.divider]} />
        <View style={[styles.marginTop20]}>
          <View style={[styles.flexDirectionRow]}>
            <Text style={{ fontWeight: 'bold', flex: 1 }}>
              {t('createEvent.eventType')}
            </Text>
            {wineTypeChanged && (
              <TouchableOpacity>
                <Text style={[styles.fontWeightBold, { flex: 0 }]}>
                  Edit Tasting Question List
                  <Icon name="pencil" type="EvilIcons" />
                </Text>
              </TouchableOpacity>
            )}
          </View>
          <View style={styles.marginTop10}>
            <DropDownPicker
              open={open}
              value={wineType}
              items={wineLists}
              setOpen={() => {
                setUsersOpen(false)
                setOpen(!open)
              }}
              setValue={setWineType}
              setItems={setWineLists}
              placeholder="Select"
              placeholderStyle={{ color: '#D7D4CD' }}
              style={{
                borderColor: error && wineType === null ? Colors.error : '#D7D4CD',
              }}
              closeAfterSelecting={true}
              itemSeparator={true}
              itemSeparatorStyle={{ backgroundColor: '#d7d4cd' }}
              showTickIcon={false}
              selectedItemContainerStyle={{
                backgroundColor: '#d7d4cd',
              }}
              listItemContainerStyle={{
                height: 50,
              }}
              dropDownContainerStyle={{
                backgroundColor: '#fff',
                borderColor: '#D7D4CD',
              }}
              arrowIconStyle={{
                width: 15,
                height: 15,
                marginRight: 0,
              }}
            />
          </View>
        </View>
        <View style={[styles.marginTop20, styles.divider]} />
        <View style={[styles.marginTop15, styles.flexDirectionRow]}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Text
              style={[
                styles.fontWeightBold,
                styles.fontColorBrown,
                styles.marginTop05,
              ]}
            >
              {t('createEvent.bringBottle')}
            </Text>
          </View>
          <View>
            <CheckBox
              disabled={false}
              value={bringBottles}
              boxType={'square'}
              hideBox={false}
              tintColors={{ true: Colors.primaryYellow }}
              onCheckColor={'#29121D'}
              onValueChange={newValue => setBringBottles(newValue)}
            />
          </View>
        </View>
        <View style={[styles.marginTop20, styles.divider]} />
        <View style={[styles.marginTop20, styles.alignCenter]}>
          <Text style={styles.fontWeightBold}>
            {t('createEvent.bottlesCount')}
          </Text>
          <Text>{t('createEvent.changeNumberLater')}</Text>
          <View style={[styles.flexDirectionRow, styles.marginTop15]}>
            {[...Array(9 - emptyBottle.length)].map((element, i) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    setBottleCount(i + 1)
                    setBottleCountChanged(true)
                    handleBottleClick(i + 1)
                  }}
                  key={`full-bottle-${i}`}
                >
                  <FullBottle style={{ marginHorizontal: 9 }} />
                </TouchableOpacity>
              )
            })}
            {[...Array(emptyBottle.length)].map((element, i) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    setBottleCount(i + 1 + bottleCount)
                    setBottleCountChanged(true)
                    handleBottleClick(i + 1 + bottleCount)
                  }}
                  key={`empty-bottle-${i}`}
                >
                  <EmptyBottle style={{ marginHorizontal: 9 }} />
                </TouchableOpacity>
              )
            })}
          </View>
          <View style={styles.marginTop15}>
            <Slider
              style={{ width: 360, height: 40 }}
              step={1}
              minimumValue={1}
              maximumValue={9}
              minimumTrackTintColor="#29121d"
              maximumTrackTintColor="#29121d"
              onValueChange={value => {
                setBottleCount(value)
                setBottleCountChanged(true)
                handleBottleClick(value)
              }}
              value={bottleCount}
            />
            <Text style={styles.marginLeft10}>{bottleCount}</Text>
          </View>
        </View>
        <View style={[styles.flex1, styles.marginTop20]}>
          <View style={styles.screenPositionBottom2}>
            <View>
              <BojoButtonYellow
                navigation={props.navigation}
                title={t('createEvent.next')}
                onClick={() => handleNextButtonClick()}
              />
            </View>
          </View>
        </View>
        <View style={styles.borderRadius10}>
          <Modal
            isVisible={helpModal}
            style={styles.borderRadius10}
            onBackdropPress={() => setHelpModal(false)}
          >
            <View style={styles.modal1}>
              <Text style={[styles.modal1Text]}>
                If you will not be participating in the tasting portion of this
                event, you may open an additional attendee spot on the invite
                list by unchecking this box.
              </Text>
            </View>
          </Modal>
        </View>
      </View>
    </Container>
  )
}
export default CreateEventScreen
