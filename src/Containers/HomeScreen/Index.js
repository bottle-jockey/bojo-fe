import React, { useRef, useState, useEffect } from 'react'
import { Text, Image, Dimensions, TouchableOpacity } from 'react-native'
import { Card, CardItem, Body, View, Button, Toast, Spinner } from 'native-base'
import AppStackWrapper from '../../Components/AppStackWrapper'
import styles from '../../Assets/styles'
import BojoOutlineBlack from '../../Assets/BojoInputs/BojoOutlineBlack'
import Calendar from '../../Assets/Images/calendar.png'
import Location from '../../Assets/Images/location.png'
import {
  CREATE_EVENT_SCREEN,
  OFFLINE_TASTING_SCREEN,
} from '../../Constants/Routes'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { useTranslation } from 'react-i18next'
import YoutubePlayer from 'react-native-youtube-iframe'
import { WebView } from 'react-native-webview'
import { useDispatch, useSelector } from 'react-redux'
import { homeArticlesAction } from '@/Store/App/Actions/HomeAction'
const { width: screenWidth } = Dimensions.get('window')

const HomeScreenContainer = props => {
  const { width: screenWidth, height: screenHeight } = Dimensions.get('window')

  const { t } = useTranslation()
  const carouselRef = useRef(null)
  const events = [{ id: 1 }, { id: 2 }, { id: 3 }]
  const [activeSlide, setActiveSlide] = useState(0)

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(homeArticlesAction())
  }, [])
  const articles = useSelector(state => state.articles)
  const [openWeb, setOpenWeb] = useState(false)
  const [selectedUrl, setSelectedUrl] = useState('')
  const [playVideo, setPlayVideo] = useState(true)

  const renderItem = ({ item, index }) => {
    return (
      <View style={{ width: '90%' }}>
        <View style={styles.eventCard}>
          <Text style={styles.fontWeightBold}>
            {t('homeScreen.partnersInWine')}
          </Text>
          <Body style={styles.marginTop05}>
            <View style={{ alignItems: 'flex-start' }} />
            <View style={styles.flexDirectionRow}>
              <View style={styles.flexHalf}>
                <View style={styles.flexDirectionRow}>
                  <Image
                    source={Calendar}
                    style={{ height: 19, width: 20, marginTop: 5 }}
                  />
                  <View style={styles.marginLeft5}>
                    <Text>Fri, March 5th</Text>
                    <Text>at 8:00 PM</Text>
                  </View>
                </View>
              </View>
              <View style={styles.flexHalf}>
                <View style={styles.flexDirectionRow}>
                  <Image
                    source={Location}
                    style={{ height: 20, width: 18, marginTop: 5 }}
                  />
                  <View style={styles.marginLeft5}>
                    <Text>213 17th Av. E, </Text>
                    <Text>Seattle WA 98112</Text>
                  </View>
                </View>
              </View>
            </View>
          </Body>
        </View>
      </View>
    )
  }

  const renderText = article => {
    return (
      <Card style={[styles.padding20, styles.shadow0, styles.borderRadius10]}>
        <View>
          <Text style={[styles.articleCardText, styles.fontWeightBold]}>
            {article.title}
          </Text>
          <View style={styles.flexDirectionRow}>
            <Text numberOfLines={3} style={{ flex: 0.8 }}>
              {article.description}
            </Text>
            <TouchableOpacity
              onPress={() => {
                setOpenWeb(true)
                setSelectedUrl(article.url)
              }}
              style={{ flex: 0.2, marginTop: 30 }}
            >
              <Text
                style={[
                  styles.underline,
                  styles.fontWeightBold,
                  styles.marginLeft5,
                ]}
              >
                {t('homeScreen.more')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Card>
    )
  }

  const renderTextWithImage = article => {
    return (
      <Card
        style={[styles.shadow0, styles.borderRadius10, styles.flexDirectionRow]}
      >
        <View
          style={[
            styles.marginTop05,
            styles.padding20,
            { flex: 1, paddingRight: 0 },
          ]}
        >
          <View>
            <Text style={[styles.articleCardText, styles.fontWeightBold]}>
              {article.title}
            </Text>
            <View style={styles.flexDirectionRow}>
              <Text numberOfLines={3} style={{ flex: 0.8 }}>
                {article.description}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  setOpenWeb(true)
                  setSelectedUrl(article.url)
                }}
                style={{ flex: 0.2, marginTop: 30 }}
              >
                <Text
                  style={[
                    styles.underline,
                    styles.fontWeightBold,
                    styles.marginLeft5,
                    { flex: 0.5 },
                  ]}
                >
                  {t('homeScreen.more')}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <CardItem style={{ flex: 0.3 }}>
          <Image
            source={{ uri: `https://bojo-dev.ngrok.io${article.cover_image}` }}
            style={{ height: 60, width: 60 }}
          />
        </CardItem>
      </Card>
    )
  }

  const renderTextWithVideo = article => {
    const videoIFrame = article.video_embed
    const start = videoIFrame.indexOf('embed')
    const videoId = videoIFrame.slice(start + 6, 79)
    return (
      <Card style={[styles.padding20, styles.shadow0, styles.borderRadius10]}>
        <View style={{ padding: 3 }}>
          <YoutubePlayer height={170} play={playVideo} videoId={videoId} />
          <Text style={[styles.articleCardText, styles.fontWeightBold]}>
            {article.title}
          </Text>
          <View style={styles.flexDirectionRow}>
            <Text numberOfLines={1} style={{ flex: 0.9 }}>
              {article.description}
            </Text>
            <TouchableOpacity
              onPress={() => {
                setOpenWeb(true)
                setSelectedUrl(article.url)
              }}
              style={{ flex: 0.2 }}
            >
              <Text
                style={[
                  styles.underline,
                  styles.fontWeightBold,
                  styles.marginLeft5,
                ]}
              >
                {t('homeScreen.more')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Card>
    )
  }

  const renderMapArticles = () => {
    if (articles.status === 'VALUE_SUCCESS') {
      return articles.data.map((article, index) => {
        return (
          <View key={index}>
            {article.type === 'text_only'
              ? renderText(article)
              : article.type === 'text_with_cover_image'
              ? renderTextWithImage(article)
              : renderTextWithVideo(article)}
          </View>
        )
      })
    } else if (articles.status === 'VALUE_FAILED') {
      Toast.show({
        text: t('somethingWentWrong'),
        buttonText: t('actions.okay'),
        type: 'danger',
        duration: 10000,
        position: 'bottom',
      })
    } else if (articles.status === 'VALUE_REQUESTED') {
      return (
        <View style={[styles.alignCenter, styles.marginTop40]}>
          <Spinner />
        </View>
      )
    }
  }

  return (
    <>
      {openWeb ? (
        <WebView source={{ uri: selectedUrl }} style={{ marginTop: 20 }} />
      ) : (
        <AppStackWrapper navigation={props.navigation}>
          <View style={[styles.bojoGrey, styles.padding20]}>
            <Text style={[styles.fontSize9, styles.fontWeightBold]}>
              {t('homeScreen.upcomingEvents')}
            </Text>
            <View>
              <Carousel
                ref={carouselRef}
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth}
                data={events}
                renderItem={renderItem}
                onSnapToItem={index => setActiveSlide(index)}
                autoplay
                autoplayInterval={2000}
                activeSlideOffset={0}
                scrollEnabled={true}
              />
              <View>
                <Pagination
                  dotsLength={events.length}
                  activeDotIndex={activeSlide}
                  containerStyle={styles.carousalDotsContainer}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                />
              </View>
            </View>
            <View style={styles.marginTop15}>
              <Button
                block
                style={styles.createEventButton}
                onPress={() => {
                  setPlayVideo(false)
                  props.navigation.navigate(CREATE_EVENT_SCREEN)
                }}
              >
                <Text style={styles.createEventButtonText}>
                  {t('homeScreen.createEvent')}
                </Text>
              </Button>
            </View>
            <View style={styles.marginTop15}>
              <BojoOutlineBlack
                title={t('homeScreen.offlineTasting')}
                onClick={() => {
                  setPlayVideo(false)
                  props.navigation.navigate(OFFLINE_TASTING_SCREEN)
                }}
                style={styles.bojoOutlineButton}
                bordered={false}
              />
            </View>
            <View style={styles.marginTop20}>{renderMapArticles()}</View>
          </View>
        </AppStackWrapper>
      )}
    </>
  )
}
export default HomeScreenContainer
