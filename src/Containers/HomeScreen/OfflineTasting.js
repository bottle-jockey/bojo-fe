import React from 'react'
import { Text } from 'react-native'
import {
  View,
  Header,
  Icon,
  Button,
  Container,
  Left,
  Right,
  Title,
  Content,
} from 'native-base'
import styles from '../../Assets/styles'

const OfflineTastingScreen = props => {
  return (
    <Container>
      <Header style={styles.header}>
        <Left>
          <Button transparent large onPress={() => props.navigation.goBack()}>
            <Icon name="arrow-back" style={{ color: '#fff' }} />
          </Button>
        </Left>
        <View style={{ justifyContent: 'center', marginLeft: 60 }}>
          <Title style={styles.headerTitle}>Offline Tasting</Title>
        </View>
        <Right>
          <Button transparent large onPress={() => props.navigation.goBack()}>
            <Icon
              name="information-outline"
              type="MaterialCommunityIcons"
              style={{ color: '#fff' }}
            />
          </Button>
        </Right>
      </Header>
      <Content>
        <View>
          <Text
            style={{ textAlign: 'center', marginTop: 20, fontWeight: 'bold' }}
          >
            Offline Tasting Screen
          </Text>
        </View>
      </Content>
    </Container>
  )
}
export default OfflineTastingScreen
