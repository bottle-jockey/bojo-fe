/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react'
import { ImageBackground } from 'react-native'
import { useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import splash from '@/Assets/Images/image.png'
import { postHeartBeatAction } from '../../Store/Auth/Actions/LoginAction'

const IndexStartupContainer = () => {
  const { t } = useTranslation()

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(postHeartBeatAction())
  }, [dispatch])

  return (
    <ImageBackground
      source={splash}
      style={{
        flex: 1,
        resizeMode: 'contain',
        height: '130%',
      }}
    />
    // <View style={[Layout.fill, Layout.colCenter]}>
    //   <Brand />
    //   <ActivityIndicator size={'large'} style={[Gutters.largeVMargin]} />
    //   <Text style={Fonts.textCenter}>{t('welcome')}</Text>
    // </View>
  )
}

export default IndexStartupContainer
