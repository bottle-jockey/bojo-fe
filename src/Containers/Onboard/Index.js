/* eslint-disable react-native/no-inline-styles */
import React, { useState, useRef, useEffect } from 'react'
import { View, Text, Dimensions } from 'react-native'
import { useTheme } from '@/Theme'
import { useTranslation } from 'react-i18next'
import { Container } from 'native-base'
import NameLogo from '@/Assets/Images/BojoNameIcon.svg'
import Styles from '@/Assets/styles'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import BojoButtonYellow from '../../Assets/BojoInputs/BojoButtonYellow'
import styles from '../../Assets/styles'
import { LOGIN_SCREEN } from '../../Constants/Routes'
import { useDispatch, useSelector } from 'react-redux'
import onboardSlice from '../../Store/Auth/OnboardSlice'
import { navigateAndSimpleReset } from '@/Navigators/Root'
const { width: screenWidth } = Dimensions.get('window')

const IndexOnboardContainer = ({ navigation }) => {
  const { t } = useTranslation()

  const data = React.useMemo(() => {
    return [
      {
        id: 1,
        text: t('onBoard.hostTasting'),
        subText: t('onBoard.inviteFriends'),
      },
      {
        id: 2,
        text: t('onBoard.joinEvents'),
        subText: t('onBoard.inviteFriends'),
      },
      {
        id: 3,
        text: t('onBoard.keepTrackOfTasting'),
        subText: t('onBoard.addOfflineTasting'),
      },
    ]
  }, [t])

  const onboardState = useSelector(state => state.onboard)
  const isOnboardComplete = onboardState.complete
  const dispatch = useDispatch()

  const { Gutters, Layout } = useTheme()
  const [activeSlide, setActiveSlide] = useState(0)

  const carouselRef = useRef(null)

  useEffect(() => {
    if (isOnboardComplete) {
      return navigateAndSimpleReset(LOGIN_SCREEN)
    }

    const timer = setTimeout(() => {
      activeSlide === 2 && navigateAndSimpleReset(LOGIN_SCREEN)
    }, 2000)
    return () => {
      if (timer) {
        clearTimeout(timer)
      }

      if (!isOnboardComplete) {
        dispatch(onboardSlice.actions.markOnboardComplete())
      }
    }
  })

  const renderItem = ({ item, index }) => {
    return (
      <View style={[Layout.fill, Layout.colCenter, Gutters.smallHPadding]}>
        <View style={[[Layout.colCenter, Gutters.smallHPadding]]}>
          <Text style={Styles.tourTitle} numberOfLines={2}>
            {item.text}
          </Text>
          <Text style={Styles.tourSubTitle} numberOfLines={2}>
            {item.subText}
          </Text>
        </View>
      </View>
    )
  }

  return (
    <Container>
      <View style={{ alignItems: 'center' }}>
        <NameLogo height={70} />
      </View>

      <View style={Styles.tourContainer}>
        <Carousel
          ref={carouselRef}
          sliderWidth={screenWidth}
          sliderHeight={screenWidth}
          itemWidth={screenWidth - 60}
          data={data}
          renderItem={renderItem}
          onSnapToItem={index => setActiveSlide(index)}
          autoplay
          autoplayInterval={2000}
          scrollEnabled={true}
        />
        {activeSlide !== data.length - 1 ? (
          <Pagination
            dotsLength={data.length}
            activeDotIndex={activeSlide}
            dotStyle={Styles.carousalDots}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
          />
        ) : (
          <View style={styles.padding20}>
            <BojoButtonYellow
              navigation={navigation}
              title={t('onBoard.howItWorks')}
              onClick={() => navigation.navigate(LOGIN_SCREEN)}
            />
          </View>
        )}
      </View>
    </Container>
  )
}
export default IndexOnboardContainer
