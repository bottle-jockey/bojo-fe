import { Toast } from 'native-base'

export const showErrorToast = ({
  message,
  duration = 5000,
  position = 'top',
  buttonText = 'Okay',
} = {}) => {
  Toast.show({
    text: message || 'Something went wrong!',
    buttonText: buttonText,
    duration,
    position,
    type: 'danger',
  })
}

export const showSuccessToast = ({
  message,
  duration = 5000,
  position = 'top',
  buttonText = 'Okay',
} = {}) => {
  Toast.show({
    text: message || 'Hurray!',
    buttonText: buttonText,
    duration,
    position,
    type: 'success',
  })
}
