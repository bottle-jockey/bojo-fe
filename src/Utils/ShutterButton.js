/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { TouchableOpacity, View } from 'react-native'

const ShutterButton = props => {
  const { onPress } = props
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          borderWidth: 2,
          borderRadius: 25,
          borderColor: 'white',
          height: 50,
          width: 50,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <View
          style={{
            borderWidth: 2,
            borderRadius: 20,
            borderColor: 'white',
            height: 40,
            width: 40,
            backgroundColor: 'white',
          }}
        />
      </View>
    </TouchableOpacity>
  )
}

export default ShutterButton
