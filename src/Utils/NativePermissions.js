import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions'
import { isIOS, isANDROID } from './CommonUtils'

/**
 *
 * @param {PERMISSIONS: react-native-permissions} permission
 */
export const checkAndRequestPermission = async (permission, shouldRequest) => {
  let status = await check(permission)
  if (shouldRequest) {
    status = await request(permission)
  }
  return status
}

export const checkCameraPermission = async (shouldRequest = true) => {
  const platformPermission = isIOS()
    ? PERMISSIONS.IOS.CAMERA
    : PERMISSIONS.ANDROID.CAMERA
  const status = await checkAndRequestPermission(
    platformPermission,
    shouldRequest,
  )
  return status === 'granted'
}

export const checkRecordAudioPermission = async (shouldRequest = true) => {
  if (isANDROID()) {
    const status = await checkAndRequestPermission(
      PERMISSIONS.ANDROID.RECORD_AUDIO,
      shouldRequest,
    )
    return status === 'granted'
  } else {
    return true // Needs to be handled for iOS.
  }
}
