import { isArray } from './CommonUtils'

export const required = value => (value ? undefined : 'is required')

export const notEmpty = value => {
  if (!value) {
    return 'is required'
  }

  if (value === '[]' || value === '{}') {
    return 'is required'
  }

  if (isArray(value)) {
    return value.length > 0 ? undefined : 'is required'
  }

  if (typeof value === 'object') {
    return value === {} ? 'is required' : undefined
  }

  return undefined
}

export const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined

const minLength = min => value =>
  value && value.length < min
    ? `Must be ${min} characters or greater`
    : undefined

export const minLength6 = minLength(6)
export const minLength8 = minLength(8)
export const maxLength12 = maxLength(12)
export const maxLength13 = maxLength(13)
export const maxLength14 = maxLength(14)

export const passwordsMustMatch = (value, allValue) =>
  value === allValue.password ? undefined : "and Passwords don't match"

export const number = value =>
  value && isNaN(Number(value)) ? 'Must be a number' : undefined

export const string = value =>
  /^[A-Za-z]+$/.test(value) ? undefined : 'is not valid'

export const minValue = min => value =>
  value && value < min ? `Must be at least ${min}` : undefined

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'is invalid'
    : undefined

export const phone = value =>
  value && !/^(\+0?1\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/.test(value)
    ? 'is invalid'
    : undefined

export const password = value =>
  value && !/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/i.test(value)
    ? 'should have atleast one lowercase, one uppercase, one numeric and one special character'
    : undefined
export const citizenId = value =>
  value && value.length !== 12 ? 'must be 12 digits' : undefined
