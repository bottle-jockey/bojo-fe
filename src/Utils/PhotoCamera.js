import React, { useState, useEffect, useRef } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import { Button, Icon } from 'native-base'
import PropTypes from 'prop-types'
import ShutterButton from './ShutterButton'
import { RNCamera as Camera } from 'react-native-camera'
import { checkCameraPermission } from './NativePermissions'

const PhotoCamera = props => {
  const { openType } = props

  const [hasPermission, setHasPermission] = useState(null)
  const [type, setType] = useState(openType)

  let cameraRef = null

  useEffect(() => {
    (async () => {
      const hasPermission = await checkCameraPermission()
      setHasPermission(hasPermission)
    })()
  }, [])

  if (hasPermission === null) {
    return <View />
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>
  }

  const flipCamera = () => {
    const cameraType =
      type === Camera.Constants.Type.back
        ? Camera.Constants.Type.front
        : Camera.Constants.Type.back
    setType(cameraType)
  }

  const captureImage = async () => {
    if (!cameraRef) {
      return
    }

    const { onSave = v => {} } = props

    const capturedImage = await cameraRef.takePictureAsync({ quality: 0.5 })
    onSave(capturedImage)
  }

  const closeCamera = () => {
    const { onClose } = props

    if (onClose) {
      onClose()
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <Camera
        ref={r => (cameraRef = r)}
        style={styles.cameraContainerStyle}
        type={type}
      >
        <View style={styles.buttonContainerStyle}>
          <Button
            onPress={flipCamera}
            iconRight
            light
            rounded
            style={{ height: 55, width: 55 }}
          >
            <Icon name="camera-reverse-outline" style={{marginLeft: 15, color: '#000'}} />
          </Button>
          <ShutterButton onPress={captureImage} />
          <Button
            onPress={closeCamera}
            iconRight
            light
            rounded
            style={{ height: 55, width: 55 }}
          >
            <Icon name="close" fontSize='25' style={{marginLeft: 15, color: '#000'}} />
          </Button>
        </View>
      </Camera>
    </View>
  )
}

const styles = StyleSheet.create({
  cameraContainerStyle: {
    flex: 1,
  },
  buttonContainerStyle: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-evenly',
    marginBottom: 10,
  },
})

PhotoCamera.propTypes = {
  onSave: PropTypes.func,
  onClose: PropTypes.func,
  openType: PropTypes.string,
}

PhotoCamera.types = {
  ...Camera.Constants.Type,
}

PhotoCamera.defaultProps = {
  onSave: photo => {},
  onClose: () => {},
  openType: Camera.Constants.Type.back,
}

export default PhotoCamera
