import { Platform } from 'react-native';

export const isIOS = () => Platform.OS === 'ios'
export const isANDROID = () => Platform.OS === 'android'
export const isArray = list => {
  return list && Object.prototype.toString.call(list) === '[object Array]'
}
export const jsonToFormData = json => {
  const formData = new FormData()

  Object.keys(json).forEach(jk => {
    formData.append(jk, json[jk])
  })

  return formData
}
