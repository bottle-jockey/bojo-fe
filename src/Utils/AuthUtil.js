import EncryptedStorage from 'react-native-encrypted-storage'
import {
  AUTH_KEY_PREFERENCE,
  REFRESH_KEY_PREFERENCE,
} from '../Constants/Preference'

export const saveTokens = async tokens => {
  const authToken = `Bearer ${tokens.access}`
  const refreshToken = `Bearer ${tokens.refresh}`

  await EncryptedStorage.setItem(AUTH_KEY_PREFERENCE, authToken)
  await EncryptedStorage.setItem(REFRESH_KEY_PREFERENCE, refreshToken)
}

export const deleteTokens = async () => {
  await EncryptedStorage.removeItem(AUTH_KEY_PREFERENCE)
  await EncryptedStorage.removeItem(REFRESH_KEY_PREFERENCE)
}
