import PropTypes from 'prop-types'
import React from 'react'
import Modal from 'react-native-modal'
import PhotoCamera from './PhotoCamera'

const CameraModal = props => {
  const { show = false, onClose = () => {} } = props
  const { onSave = photo => {}, openType = PhotoCamera.types.back } = props

  return (
    <Modal isVisible={show} style={{ height: '100%' }}>
      <PhotoCamera onClose={onClose} onSave={onSave} openType={openType} />
    </Modal>
  )
}
CameraModal.propTypes = {
  onClose: PropTypes.func,
  onSave: PropTypes.func,
  openType: PropTypes.any,
  show: PropTypes.bool,
}

export default CameraModal
