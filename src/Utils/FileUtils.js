import { Alert } from 'react-native'
import moment from 'moment'
import FileViewer from 'react-native-file-viewer'
import DocumentPicker, {
  DocumentPickerOptions,
} from 'react-native-document-picker'
// import RNShare from 'react-native-share'

import { isIOS, isExternalUri, isANDROID } from './CommonUtils'

const RNFS = require('react-native-fs')

const INVALID_URL = 'Invalid URL'
const DEFAULT_EXTENSION = 'jpg'

const checkFileNameForExtension = (fileName = '') => {
  if (!fileName) {
    return false
  }
  if (!fileName.includes('.')) {
    return false
  }

  const assumedExtension = fileName.split('.').slice(-1)[0]
  return Object.values(FILE_EXTENSIONS).includes(assumedExtension)
}

/**
 * Returns cached file path without extension
 * @param {string} fileName
 */
const getCachedFileNameFor = (fileName, extension = DEFAULT_EXTENSION) => {
  const isFileContainsExtensions = checkFileNameForExtension(fileName)
  const _fileName =
    fileName + (isFileContainsExtensions ? '' : `.${extension}`) ||
    `ensource_doc_${moment.now()}.${extension}`

  const cachePath = 'file://' + RNFS.CachesDirectoryPath + '/' + _fileName
  return [cachePath, _fileName]
}

/**
 * Assuming the end part of an url contains file extension
 * @param {string} url
 */
const getExtensionFromURL = url => {
  let extension = DEFAULT_EXTENSION

  const urlSplits = url?.split('/').slice(-1)
  if (urlSplits && urlSplits[0].includes('.')) {
    const urlExtensionSplit = urlSplits[0]?.split('.').slice(-1)
    if (urlExtensionSplit && urlExtensionSplit.length > 0) {
      extension = urlExtensionSplit[0]
    }
  }

  return extension
}

export const FILE_EXTENSIONS = {
  JPG: 'jpg',
  JPEG: 'jpeg',
  PNG: 'png',
  MP4: 'mp4',
  TXT: 'txt',
  PDF: 'pdf',
}

/**
 * Download file from external source and saves in cache
 * @param {string} url
 * @param {string} fileName
 * @param {Headers} headers
 */
export const downloadFile = async (url, fileName, headers) => {
  if (!url) {
    Alert.alert(INVALID_URL)
    return
  }

  const fileExtension = getExtensionFromURL(url)
  const [toFile] = getCachedFileNameFor(fileName, fileExtension)
  const downloadFileOptions = {
    fromUrl: url,
    toFile,
    headers,
  }

  const { jobId, promise } = RNFS.downloadFile(downloadFileOptions)
  const { statusCode } = await promise

  if (statusCode && statusCode === 200) {
    return toFile
  }
  return null // if the download failed.
}

export const saveBase64File = async (
  imageData,
  fileName,
  extension = DEFAULT_EXTENSION,
) => {
  const MAX_RETRIES = 20
  let [filePath, name] = getCachedFileNameFor(fileName, extension),
    retryCount = 1

  while (retryCount <= MAX_RETRIES && (await RNFS.exists(filePath))) {
    [filePath, filename] = getCachedFileNameFor(
      fileName + `(${retryCount})`,
      extension,
    )
    retryCount++
  }

  await RNFS.writeFile(filePath, imageData, 'base64')

  const stat = await RNFS.stat(filePath)

  return { uri: filePath, name, size: stat.size }
}

export const viewFileFromSystem = async (
  fileURI,
  filename = null,
  headers = null,
) => {
  const invalidFile = 'Invalid File ' + fileURI

  if (isExternalUri(fileURI)) {
    fileURI = await downloadFile(fileURI, filename, headers)
  }
  if (!fileURI) {
    Alert.alert(invalidFile)
  }

  const stat = await RNFS.stat(fileURI)
  const localFile = isIOS() ? stat.path : stat.originalFilepath
  await FileViewer.open(localFile, { showOpenWithDialog: true })

  return true
}

/**
 * Picks a document or throws Exception.
 * @param {DocumentPickerOptions} options
 */
export const pickDocument = async (options = {}) => {
  const res = await DocumentPicker.pick(options)

  if (isANDROID()) {
    const [localFilePath] = getCachedFileNameFor(res.name)
    await RNFS.copyFile(res.uri, localFilePath) // To convert content:// to file:// as expected for upload file
    // const stat = await RNFS.stat(localFilePath);

    return {
      ...res,
      uri: localFilePath,
    }
  }

  return res
}

export const getFileStat = async fileURI => {
  return await RNFS.stat(fileURI)
}
