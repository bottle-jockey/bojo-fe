import { bojoAPI, METHODS } from '../Api'
import { GET_ARTICLES } from '@/Constants/Endpoints'

export const getArticles = async data => {
  const response = await bojoAPI(GET_ARTICLES)
  return response
}
