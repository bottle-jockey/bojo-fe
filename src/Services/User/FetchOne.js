import api, { handleError } from '@/Services'

export default async userId => {
  if (!userId) {
    return handleError({ message: 'User ID is required' })
  }

  try {
    const response = await api.get(`users/${userId}`)
    return response.data
  } catch (e) {
    // return handleError({ message: e.toString() })
  }
}
