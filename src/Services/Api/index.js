import EncryptedStorage from 'react-native-encrypted-storage'
import { AUTH_KEY_PREFERENCE } from '@/Constants/Preference'
import api from '../'

/**
 * @typedef {object} BojoOptions
 * @property {METHODS} options.method
 * @property {object} options.body
 * @property {object} options.pathParas
 * @property {object} options.queryParams
 * @property {bool} options.isAuthenticated
 * @property {import('axios').AxiosRequestConfig} options.axiosOptions
 * @property {*} options.shouldCache
 */

export const METHODS = {
  POST: 'POST',
  GET: 'GET',
  DELETE: 'DELETE',
  PUT: 'PUT',
}

const DEFAULT_API_OPTIONS = {
  isAuthenticated: true,
  method: METHODS.GET,
  shouldCache: false,
}

/**
 * To connect with BOJO B.E
 * @param {string} url
 * @param {BojoOptions} options
 */
export const bojoAPI = async (url, options) => {
  options = {
    ...DEFAULT_API_OPTIONS,
    ...options,
  }

  let { method, pathParams, body } = options

  url = prepareURL(url, pathParams)
  const axiosOptions = await prepareAxiosOptions(options)

  const isDev = process.env.NODE_ENV === 'development'
  if (isDev) {
    console.log(''.repeat(50))
    console.log('[ URL ]: ', url)
    console.log('[ OPTIONS ] :>> ', options)
    console.log('[ HEADERS ]: ', axiosOptions.headers)
  }

  /** @type {import('axios').AxiosResponse} */
  let res
  try {
    switch (method) {
      case METHODS.GET:
        res = await api.get(url, axiosOptions)
        break
      case METHODS.POST:
        res = await api.post(url, body, axiosOptions)
        break
      case METHODS.PUT:
        res = await api.put(url, body, axiosOptions)
        break
      case METHODS.DELETE:
        res = await api.delete(url, axiosOptions)
        break
      default:
        break
    }

    if (isDev) {
      console.log('[ RESPONSE ]: ', res)
    }

    if (isSuccessResponse(res)) {
      return res.data
    } else {
      throw res.data
    }
  } catch (e) {
    if (isDev) {
      console.log('e :>> ', e)
    }
    if (e.data && e.data.error) {
      throw e.data.error.message
    } else {
      throw e
    }
  }
}

/**
 *
 * @param {BojoOptions} options
 */
const prepareAxiosOptions = async options => {
  const { isAuthenticated, axiosOptions = {} } = options

  // HANDLE AUTH
  if (isAuthenticated) {
    const authToken = await EncryptedStorage.getItem(AUTH_KEY_PREFERENCE)
    axiosOptions.headers = {
      Authorization: authToken,
    }
  }

  return axiosOptions
}

/**
 *
 * @param {string} url
 * @param {object} pathParams
 * @returns parsed URL String
 */
const prepareURL = (url, pathParams) => {
  if (!pathParams) {
    return url
  }

  if (typeof pathParams !== 'object') {
    return url
  }

  const keys = Object.keys(pathParams)
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i]
    url = url.replace(`:${key}`, pathParams[key])
  }

  return url
}

/**
 * @param {import('axios').AxiosResponse} res
 */
const isSuccessResponse = res => {
  const status = res.status
  return status >= 200 && status < 300
}
