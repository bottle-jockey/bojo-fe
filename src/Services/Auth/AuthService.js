import { bojoAPI, METHODS } from '../Api'
import { POST_HEART_BEAT, POST_LOGIN } from '@/Constants/Endpoints'
import {
  API_ACCOUNT,
  POST_PASSWORD_RESET,
  POST_PASSWORD_RESET_CONFIRM,
  POST_SIGN_UP,
  POST_VERIFY_PHONE,
} from '../../Constants/Endpoints'

export const postLogin = async data => {
  const response = await bojoAPI(POST_LOGIN, {
    isAuthenticated: false,
    body: data,
    method: METHODS.POST,
  })

  return response
}

export const postHeartBeat = async data => {
  const response = await bojoAPI(POST_HEART_BEAT, {
    method: METHODS.POST,
    body: data,
  })

  return response
}

export const postSignup = async data => {
  const response = await bojoAPI(POST_SIGN_UP, {
    method: METHODS.POST,
    body: data,
  })

  return response
}

export const postVerifyPhone = async data => {
  const response = await bojoAPI(POST_VERIFY_PHONE, {
    method: METHODS.POST,
    body: data,
  })

  return response
}

export const putAccount = async data => {
  const response = await bojoAPI(API_ACCOUNT, {
    method: METHODS.PUT,
    body: data,
  })

  return response
}

export const postPasswordReset = async data => {
  const response = await bojoAPI(POST_PASSWORD_RESET, {
    method: METHODS.POST,
    body: data,
  })

  return response
}

export const postPasswordResetConfirm = async data => {
  const response = await bojoAPI(POST_PASSWORD_RESET_CONFIRM, {
    method: METHODS.POST,
    body: data,
  })

  return response
}
