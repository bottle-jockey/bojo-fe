/* eslint-disable curly */
import axios from 'axios'
import handleError from '@/Services/utils/handleError'
import { Config } from '@/Config'

const instance = axios.create({
  baseURL: Config.API_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 3000,
})

instance.interceptors.response.use(
  response => response,
  ({ message, response: { data, status } = {} }) => {
    if (data && data.status > 300) {
      if (data.error) {
        const parsedMessage = formatErrorMessage(data.error)
        data.error.message = parsedMessage
      }
    }

    if (data && !data.error) {
      data.error = {
        message: data.detail || message,
      }
    }

    return handleError({ message, data, status })
  },
)

const formatErrorMessage = errorObj => {
  if (!errorObj) return null
  if (errorObj.message) return errorObj.message

  const errorKeys = Object.keys(errorObj)
  if (errorKeys.length === 0) return 'Something went wrong'

  const firstKey = errorKeys[0]
  const firstError = errorObj[firstKey]
  return `${firstKey}: ${firstError}`
}

export default instance
