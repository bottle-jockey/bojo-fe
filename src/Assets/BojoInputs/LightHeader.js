import React from 'react'
import { Header, Left, Body, Button, Right, Icon } from 'native-base'
import NameLogo from '@/Assets/Images/BojoNameIcon.svg'

const LightHeader = props => {
  const { actions } = props

  return (
    <Header transparent style={{ marginTop: 20 }}>
      {props.left ? (
        <Left>
          <Button transparent large onPress={() => props.navigation.goBack()}>
            <Icon name="arrow-back" style={{ color: '#29121D' }} />
          </Button>
        </Left>
      ) : (
        <Left />
      )}
      <Body style={{ marginRight: 60 }}>
        <NameLogo height={80} />
      </Body>
      {actions ? <Right>{actions}</Right> : <Right />}
    </Header>
  )
}
export default LightHeader
