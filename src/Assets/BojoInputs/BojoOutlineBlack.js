/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import { Button, Text } from 'native-base'
import styles from '../styles'
const BojoOutlineBlack = props => {
  return (
    <Button
      block
      bordered={props.bordered}
      style={[props.style]}
      onPress={props.onClick}
    >
      <Text style={styles.bojoYellowButtonText}>{props.title}</Text>
    </Button>
  )
}
export default BojoOutlineBlack
