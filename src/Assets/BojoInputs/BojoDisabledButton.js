import React from 'react';
import { Button, Text } from 'native-base';
const BojoDisabledButton = (props) => {
  return (
    <Button disabled block style={{borderRadius: 10, margin: 15}}>
        <Text style={{ color: "#29121D", fontWeight: "bold", fontSize: 18, textTransform: "capitalize"}}>{props.title}</Text>
    </Button>
  );
}
export default BojoDisabledButton