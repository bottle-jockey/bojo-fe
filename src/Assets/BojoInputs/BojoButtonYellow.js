import React from 'react'
import { Button, Spinner, Text, View } from 'native-base'

import styles from '../styles'

const BojoButtonYellow = props => {
  const renderLoader = () => {
    return (
      <View>
        <Spinner />
      </View>
    )
  }

  const renderMainContent = () => {
    if (props.loading) {
      return renderLoader()
    }

    return <Text style={styles.bojoYellowButtonText}>{props.title}</Text>
  }

  return (
    <Button
      block
      disabled={props.loading || props.disabled}
      style={(props.style, styles.bojoYellowButton)}
      onPress={props.onClick}
    >
      {renderMainContent()}
    </Button>
  )
}

export default BojoButtonYellow
