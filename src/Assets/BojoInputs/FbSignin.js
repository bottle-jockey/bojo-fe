import React from 'react'
import { Image } from 'react-native'
import facebookIcon from '../Images/facebook.png'
import { Button, Text, Icon } from 'native-base'
import styles from '../styles'

const FbSignInButton = props => {
  return (
    <Button bordered light block style={styles.buttonOutline}>
      <Image source={facebookIcon} />
      <Text style={styles.buttonText}>Sign in with Facebook</Text>
      <Icon style={styles.rightArrow} active name="arrow-forward" />
    </Button>
  )
}
export default FbSignInButton
