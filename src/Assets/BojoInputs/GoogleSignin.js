import React from 'react'
import { Image } from 'react-native'
import googleIcon from '../Images/google.png'
import { Button, Text, Icon } from 'native-base'
import styles from '../styles'

const GoogleSigninButton = props => {
  return (
    <Button bordered light block style={styles.buttonOutline}>
      <Image width={5} source={googleIcon} />
      <Text style={styles.buttonText}>Sign in with Google</Text>
      <Icon style={styles.rightArrow} active name="arrow-forward" />
    </Button>
  )
}
export default GoogleSigninButton
