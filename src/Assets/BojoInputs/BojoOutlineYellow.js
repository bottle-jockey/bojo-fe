import  React from 'react'
import { Button, Text } from 'native-base'
import { Colors } from '@/Theme/Variables'

const BojoOutlineYellow = (props) => {
  return (
    <Button block bordered style={{borderRadius: 10, margin: 15, backgroundColor: Colors.primaryYellow}}>
      <Text style={{ fontWeight: "bold", textTransform: "capitalize", fontSize: 18 }}>{props.title}</Text>
    </Button>
  );
}
export default BojoOutlineYellow