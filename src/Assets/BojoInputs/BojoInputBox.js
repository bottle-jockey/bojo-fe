import React from 'react'
import { Input, Item } from 'native-base'
import styles from '../styles'
const BojoInputBox = props => {
  return (
    <Item
      style={
        props.touched && props.error
          ? styles.bojoInputError
          : styles.bojoInputValid
      }
      regular
    >
      <Input
        placeholder={props.placeholder}
        placeholderTextColor="#aaa"
        value={props.value}
        onChangeText={props.onChangeText}
        secureTextEntry={props.password}
        keyboardType={props.keyboardType}
        textContentType={props.textContentType}
        style={[styles.bojoInputText, props.styles]}
      />
    </Item>
  )
}
export default BojoInputBox
