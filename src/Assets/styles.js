import { PixelRatio, StyleSheet } from 'react-native'
import { FONT_FAMILY } from '../Theme/Fonts'
import { Colors } from '../Theme/Variables'

export default StyleSheet.create({
  /** General Styles */
  alignCenter: { alignItems: 'center' },
  padding5: { padding: 5 },
  padding10: { padding: 10 },
  padding15: { padding: 15 },
  padding20: { padding: 20 },
  padding30: { padding: 30 },
  paddingHorizontal25: { paddingHorizontal: 25 },
  paddingVertical25: { paddingVertical: 25 },
  paddingVertical10: { paddingVertical: 10 },
  marginTop05: { marginTop: 5 },
  marginTop10: { marginTop: 10 },
  marginTop15: { marginTop: 15 },
  marginTop20: { marginTop: 20 },
  marginTop30: { marginTop: 30 },
  marginTop40: { marginTop: 40 },
  marginTop50: { marginTop: 50 },
  marginTop80: { marginTop: 80 },
  marginLeft5: { marginLeft: 5 },
  marginLeft10: { marginLeft: 10 },
  marginLeft20: { marginLeft: 20 },
  marginLeft30: { marginLeft: 30 },
  marginLeft40: { marginLeft: 40 },
  fontWeightBold: { fontWeight: 'bold' },
  flexDirectionRow: { flexDirection: 'row' },
  flex1: { flex: 1 },
  flexHalf: { flex: 0.5 },
  bojoBackground: { backgroundColor: Colors.primary },
  bojoGrey: { backgroundColor: '#ececef' },
  fontSize9: { fontSize: 10 },
  fontSize18: { fontSize: 18 },
  underline: { textDecorationLine: 'underline' },
  shadow0: { shadowColor: 'transparent' },
  fontColorWhite: { color: '#fff' },
  fontColorBrown: { color: Colors.primary },
  fontColorYellow: { color: Colors.primaryYellow },
  borderRadius10: { borderRadius: 10 },
  borderBlack: { borderColor: '#000' },
  errorText: {
    color: Colors.error,
    fontSize: 12,
    fontFamily: FONT_FAMILY.calibri,
  },
  /** */

  /**  Tour Pages */

  tourContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  tourTitle: {
    textAlign: 'center',
    fontFamily: FONT_FAMILY.freightBolPro,
    fontSize: 32,
    lineHeight: 40,
    color: Colors.primary,
  },
  tourSubTite: {
    textAlign: 'center',
    fontFamily: FONT_FAMILY.calibri,
    fontSize: 14,
    lineHeight: 20,
  },
  carousalDots: {
    width: 8,
    height: 8,
    borderRadius: 5,
    marginHorizontal: 5,
    backgroundColor: 'rgba(0, 0, 0, 0.92)',
  },
  tourButton: {
    borderRadius: 10,
    margin: 15,
  },
  tourButtonText: {
    color: 'black',
    fontFamily: FONT_FAMILY.calibriBold,
    textTransform: 'capitalize',
    fontSize: 18,
  },

  /** */

  /** Login */
  loginContainer: {
    backgroundColor: Colors.primary,
    padding: 20,
  },
  welcome: {
    color: '#fff',
    fontFamily: FONT_FAMILY.freightBolPro,
    fontSize: PixelRatio.getPixelSizeForLayoutSize(16),
  },
  subtext: {
    color: '#fff',
    fontFamily: FONT_FAMILY.calibri,
    lineHeight: PixelRatio.getPixelSizeForLayoutSize(7),
    fontSize: PixelRatio.getPixelSizeForLayoutSize(5),
  },
  forgotPassword: {
    color: '#fff',
    fontFamily: FONT_FAMILY.calibriBold,
    fontSize: 14,
  },
  createAccount: {
    color: Colors.primaryYellow,
    fontFamily: FONT_FAMILY.calibriBold,
    fontSize: 14,
    marginTop: 10,
  },
  screenPositionBottom1: { flex: 1, padding: 20 },
  screenPositionBottom2: { flex: 1, justifyContent: 'flex-end' },

  /** */

  /** Bojo Input */
  bojoInputValid: {
    borderRadius: 10,
    backgroundColor: '#fff',
    height: 50,
    borderColor: '#ececec',
  },
  bojoInputError: {
    borderRadius: 10,
    backgroundColor: '#fff',
    height: 50,
    borderColor: Colors.error,
  },
  bojoInputText: {
    fontFamily: FONT_FAMILY.calibri,
    textAlign: 'center',
    fontSize: 12,
  },
  /** */

  /** Bojo Buttons */
  bojoYellowButtonText: {
    color: Colors.primary,
    fontWeight: 'bold',
    fontFamily: FONT_FAMILY.calibriBold,
    textTransform: 'capitalize',
    fontSize: 18,
  },
  bojoYellowButton: {
    borderRadius: 10,
    height: 44,
    backgroundColor: Colors.primaryYellow,
  },
  bojoOutlineButton: {
    borderRadius: 10,
    height: 44,
    backgroundColor: '#fff',
    shadowColor: 'transparent',
  },
  bojoOutlineBlackButton: {
    borderRadius: 10,
    height: 44,
    borderWidth: 3,
    borderColor: '#000',
  },
  /** */

  /** Social Auth sign in - FB, Google */
  buttonOutline: {
    flexDirection: 'row',
    borderRadius: 10,
    paddingLeft: 15,
    height: 50,
  },
  buttonText: {
    color: '#fff',
    flex: 0.7,
    textAlign: 'center',
    textTransform: 'capitalize',
    fontSize: 12,
  },
  rightArrow: { color: '#fff', flex: 0.2, textAlign: 'right' },
  /** */

  /** Forgot Password */
  forgotPasswordTitle: {
    color: Colors.primary,
    fontFamily: FONT_FAMILY.freightBolPro,
    fontSize: 40,
  },
  forgotPasswordSubtitle: {
    color: Colors.primary,
    fontFamily: FONT_FAMILY.calibri,
    textAlign: 'center',
    marginTop: 20,
    fontSize: 14,
  },
  forgotPasswordEmail: {
    color: Colors.primary,
    fontFamily: FONT_FAMILY.calibri,
    textAlign: 'center',
    marginTop: 20,
    fontSize: 20,
  },
  /** */

  /** Sign Up */
  signUpTitle: {
    color: Colors.primary,
    fontFamily: FONT_FAMILY.freightBolPro,
    fontSize: 40,
  },
  signUpSubtitle: {
    color: Colors.primary,
    fontFamily: FONT_FAMILY.calibri,
    textAlign: 'center',
    marginTop: 10,
    fontSize: 12,
  },
  modal1: {
    backgroundColor: '#fff',
    height: 'auto',
    padding: 20,
    justifyContent: 'center',
    borderRadius: 10,
  },
  modal1Text: {
    color: Colors.primary,
    textAlign: 'center',
    fontSize: 15,
    fontFamily: FONT_FAMILY.calibriBold,
  },
  modal1buttonRow: { flexDirection: 'row', marginTop: 40 },
  modal1button: { flex: 0.5, marginLeft: 10 },
  modal2: {
    backgroundColor: '#fff',
    height: 'auto',
    padding: 20,
    borderRadius: 10,
  },
  modal2Title: {
    marginTop: 10,
    textAlign: 'center',
    fontSize: 15,
    fontFamily: FONT_FAMILY.calibriBold,
    fontWeight: 'bold',
  },
  modal2Subtitle: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 12,
    fontFamily: FONT_FAMILY.calibriBold,
  },
  label: {
    textAlign: 'center',
    fontFamily: FONT_FAMILY.calibriBold,
    fontSize: 12,
    marginBottom: 10,
  },
  uploadModal: {
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 15,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  uploadModalText: {
    marginLeft: 10,
    marginTop: 8,
    fontFamily: FONT_FAMILY.calibriBold,
    fontWeight: 'bold',
  },
  uploadButton: {
    borderRadius: 10,
    width: '100%',
    borderColor: '#d7d4cd',
    height: 50,
  },
  /** */

  /** Sidebar */
  sideBarText: {
    fontFamily: FONT_FAMILY.calibri,
    // fontSize: PixelRatio.getPixelSizeForLayoutSize(6)
    fontSize: 14,
  },
  /** */

  /** Home Screen */
  eventCard: {
    shadowColor: 'transparent',
    borderWidth: 1,
    borderRadius: 10,
    padding: 15,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(41,18,29,0.15)',
    borderColor: 'rgba(41,18,29,0.25)',
  },
  createEventButton: {
    borderRadius: 10,
    height: 60,
    backgroundColor: Colors.primaryYellow,
  },
  createEventButtonText: {
    color: Colors.primary,
    fontFamily: FONT_FAMILY.freightBolPro,
    fontSize: 24,
  },
  articleCardText: {
    color: Colors.primary,
    textAlign: 'left',
    fontSize: 15,
    fontFamily: FONT_FAMILY.calibriBold,
  },
  /** */

  /** Membership */
  membershipText: {
    color: '#fff',
    fontFamily: FONT_FAMILY.freightBolPro,
    fontSize: 40,
    textAlign: 'center',
    paddingHorizontal: 40,
  },
  /** */

  /** Create Event */
  header: { backgroundColor: Colors.primary, height: 90 },
  headerTitle: {
    fontFamily: FONT_FAMILY.freightBolPro,
    fontSize: PixelRatio.getPixelSizeForLayoutSize(9),
    color: '#fff',
    textTransform: 'capitalize',
  },
  carousalDotsContainer: {
    flexDirection: 'row',
    height: 15,
    paddingTop: 0,
    paddingBottom: 0,
  },
  membershipBg: {
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 10,
  },

  /** Create Event  */
  borderBottom: { borderBottomWidth: 2 },
  divider: { borderTopWidth: 1, borderTopColor: '#D7D4CD' },
  eventDetailsCard: {
    shadowColor: 'transparent',
    borderWidth: 1,
    borderRadius: 10,
    padding: 15,
    marginTop: 5,
    marginBottom: 5,
    borderColor: 'rgba(41,18,29,0.25)',
  },
  bordered: {
    borderWidth: 1,
    borderColor: '#D7D4CD',
    borderRadius: 10,
    marginTop: 5,
  },
  /** */
})
